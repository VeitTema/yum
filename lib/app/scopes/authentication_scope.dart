import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

import 'package:yum/domain/auth/blocks/phone_auth_bloc.dart';

class AuthenticationScope extends AutoRouter {
  const AuthenticationScope({super.key});

  @override
  Widget Function(BuildContext context, Widget content)? get builder {
    return (context, content) {
      return MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => PhoneAuthBloc(
              phoneAuthService: GetIt.I(),
            ),
          ),
        ],
        child: content,
      );
    };
  }
}
