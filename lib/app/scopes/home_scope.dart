import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScope extends AutoRouter {
  const HomeScope({super.key});

  @override
  Widget Function(BuildContext context, Widget content)? get builder {
    return (context, content) {
      return MultiBlocProvider(
        providers: const [
          // BlocProvider(
          //   create: (context) => CartCubit(),
          // ),
        ],
        child: content,
      );
    };
  }
}
