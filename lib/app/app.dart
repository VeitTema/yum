import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:yum/presentation/app/colors.dart';
import 'package:yum/presentation/app/themes.dart';
import 'package:yum/presentation/app/typography.dart';
import 'package:yum/presentation/routes/main_router.dart';

class App extends StatefulWidget {
  const App({super.key, this.observersBuilder});

  @protected
  final List<NavigatorObserver> Function()? observersBuilder;

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  final _appRouter = MainRouter();

  @override
  Widget build(BuildContext context) {
    return AppColors(
      data: const AppColorsData.light(),
      child: ScreenUtilInit(
        designSize: const Size(375, 812),
        minTextAdapt: true,
        splitScreenMode: true,
        builder: (context, child) {
          return MaterialApp.router(
            debugShowCheckedModeBanner: false,
            routerDelegate: AutoRouterDelegate.declarative(
              _appRouter,
              routes: (_) {
                return const [HomeFlow()];
              },
              navigatorObservers: widget.observersBuilder ??
                  AutoRouterDelegate.defaultNavigatorObserversBuilder,
            ),
            theme: MaterialTheme.from(AppColors.of(context)),
            routeInformationParser: _appRouter.defaultRouteParser(),
            builder: (context, child) {
              return MediaQuery(
                data: MediaQuery.of(context).copyWith(textScaleFactor: 1),
                child: AppTypography(
                  child: child ?? const SizedBox.shrink(),
                ),
              );
            },
          );
        },
      ),
    );
  }
}
