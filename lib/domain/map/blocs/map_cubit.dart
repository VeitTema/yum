import 'dart:async';
import 'dart:convert';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yum/app/helpers/async_data.dart';
import 'package:yum/domain/map/models/point.dart';
import 'package:yum/domain/map/models/points_info.dart';
import 'package:yum/domain/map/services/map_service.dart';

typedef MapState = AsyncData<PointsInfo, Object>;

class MapCubit extends Cubit<MapState> {
  MapCubit({required this.mapService}) : super(AsyncData.initial(PointsInfo()));

  final MapService mapService;

  Future<void> initial() async {
    try {
      emit(state.inLoading());

      final prefs = await SharedPreferences.getInstance();
      final savedPointAsString = prefs.getString('deliveryPoint');
      if (savedPointAsString != null) {
        final savedPoint = Point.fromMap(
          jsonDecode(savedPointAsString) as Map<String, dynamic>,
        );

        emit(
          MapState.success(
            PointsInfo(savedPoint: savedPoint, selectedPoint: savedPoint),
          ),
        );
      } else {
        emit(
          MapState.success(
            PointsInfo(),
          ),
        );
      }
    } on Object catch (_) {
      emit(state.inFailure());
      rethrow;
    }
  }

  Future<void> fetchPoints() async {
    try {
      emit(
        MapState.loading(
          PointsInfo(
            savedPoint: state.payload.savedPoint,
            selectedPoint: state.payload.selectedPoint,
          ),
        ),
      );

      final result = await mapService.fetchPoints();

      emit(
        result.when(
          error: (e) => state.inFailure(),
          value: (pointList) {
            return MapState.success(
              PointsInfo(
                pointList: pointList,
                selectedPoint: state.payload.selectedPoint,
                savedPoint: state.payload.savedPoint,
              ),
            );
          },
        ),
      );
    } on Object catch (_) {
      emit(state.inFailure());
      rethrow;
    }
  }

  Future<void> selectPoint(Point point) async {
    emit(
      MapState.success(
        PointsInfo(
          selectedPoint: point,
          pointList: state.payload.pointList,
          savedPoint: state.payload.savedPoint,
        ),
      ),
    );
  }

  Future<void> saveSelectedPoint(Point point) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      await prefs.setString(
        'deliveryPoint',
        jsonEncode(
          point.toMap(),
        ),
      );

      emit(
        MapState.success(
          PointsInfo(
            selectedPoint: point,
            pointList: state.payload.pointList,
            savedPoint: point,
          ),
        ),
      );
    } on Object catch (_) {
      emit(state.inFailure());
      rethrow;
    }
  }
}
