import 'package:yum/app/helpers/result.dart';
import 'package:yum/domain/map/models/point.dart';

abstract class MapService {
  const MapService();

  Future<Result<ErrorLoadingPoints, List<Point>>> fetchPoints();
}

enum ErrorLoadingPoints { unknown }
