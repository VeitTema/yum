import 'package:google_maps_flutter/google_maps_flutter.dart';

class Point {
  Point({
    required this.id,
    required this.address,
    required this.location,
  });

  factory Point.fromMap(Map<String, dynamic> map) {
    return Point(
      id: map['id'] as String,
      address: map['address'] as String,
      location: LatLng.fromJson(map['location']) as LatLng,
    );
  }
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'address': address,
      'location': location,
    };
  }

  String id;
  String address;

  LatLng location;
  static final pointList = [
    {
      'id': '1',
      'address': 'Минск, ст.м. Спортивная',
      'location': [53.908593409622355, 27.480865087692468],
    },
    {
      'id': '2',
      'address': 'Минск, ул. Сурганова 57',
      'location': [53.928622694923234, 27.58273707657485],
    },
    {
      'id': '3',
      'address': 'Минск, ст.м. Академия Наук',
      'location': [53.92179680511739, 27.597904709929253],
    },
    {
      'id': '4',
      'address': 'Минск, пр. Независимости 21',
      'location': [53.90035630436463, 27.558028159700296],
    },
    {
      'id': '5',
      'address': 'Минск, ст.м. Площадь Франтишка Богушевича',
      'location': [53.89639714245639, 27.537773575578225],
    },
  ];

  static final points = [
    Point(
      id: '1',
      address: 'Минск, ст.м. Спортивная',
      location: const LatLng(53.908593409622355, 27.480865087692468),
    ),
    Point(
      id: '2',
      address: 'Минск, ул. Сурганова 57',
      location: const LatLng(53.928622694923234, 27.58273707657485),
    ),
    Point(
      id: '3',
      address: 'Минск, ст.м. Академия Наук',
      location: const LatLng(53.92179680511739, 27.597904709929253),
    ),
    Point(
      id: '4',
      address: 'Минск, пр. Независимости 21',
      location: const LatLng(53.90035630436463, 27.558028159700296),
    ),
    Point(
      id: '5',
      address: 'Минск, ст.м. Площадь Франтишка Богушевича',
      location: const LatLng(53.89639714245639, 27.537773575578225),
    ),
  ];
}
