import 'package:yum/domain/map/models/point.dart';

class PointsInfo {
  PointsInfo({
    this.selectedPoint,
    this.savedPoint,
    this.pointList = const <Point>[],
  });

  final List<Point> pointList;
  final Point? selectedPoint;
  final Point? savedPoint;
}
