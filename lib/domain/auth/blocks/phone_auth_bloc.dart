import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:yum/domain/auth/services/phone_auth_service.dart';

part 'phone_auth_bloc.freezed.dart';

@freezed
class PhoneAuthEvent with _$PhoneAuthEvent {
  const factory PhoneAuthEvent.sendOtpToPhone({
    required String phoneNumber,
  }) = _SendOtpToPhone;

  const factory PhoneAuthEvent.verifySentOtp({
    required String otpCode,
    required String verificationId,
  }) = _VerifySentOtp;

  const factory PhoneAuthEvent.onPhoneOtpSent({
    required String verificationId,
    required int? token,
  }) = _OnPhoneOtpSent;

  const factory PhoneAuthEvent.onPhoneAuthErrorEvent({
    required String error,
  }) = _OnPhoneAuthErrorEvent;

  const factory PhoneAuthEvent.onPhoneAuthVerificationCompleteEvent({
    required PhoneAuthCredential credential,
  }) = _OnPhoneAuthVerificationCompleteEvent;
}

@freezed
class PhoneAuthState with _$PhoneAuthState {
  const factory PhoneAuthState.initial() = InitialPhoneAuthState;
  const factory PhoneAuthState.loading() = LoadingPhoneAuthState;

  const factory PhoneAuthState.phoneAuthError({
    required String error,
  }) = PhoneAuthError;

  const factory PhoneAuthState.inputCode({required String verificationId}) =
      InputCodeState;

  const factory PhoneAuthState.phoneAuthVerified() = PhoneAuthVerifiedState;
}

class PhoneAuthBloc extends Bloc<PhoneAuthEvent, PhoneAuthState> {
  PhoneAuthBloc({required this.phoneAuthService})
      : super(const PhoneAuthState.initial()) {
    on<_SendOtpToPhone>(_onSendOtp);
    on<_VerifySentOtp>(_onVerifyOtp);
    on<_OnPhoneOtpSent>(
      (event, emit) =>
          emit(InputCodeState(verificationId: event.verificationId)),
    );
    on<_OnPhoneAuthErrorEvent>(
      (event, emit) => emit(PhoneAuthError(error: event.error)),
    );
    on<_OnPhoneAuthVerificationCompleteEvent>(_loginWithCredential);
  }

  @protected
  final PhoneAuthService phoneAuthService;

  Future<void> _onSendOtp(
    _SendOtpToPhone event,
    Emitter<PhoneAuthState> emit,
  ) async {
    emit(const LoadingPhoneAuthState());
    try {
      final phone = event.phoneNumber;
      emit(const LoadingPhoneAuthState());

      await phoneAuthService.verifyPhone(
        phoneNumber: phone,
        verificationCompleted: (PhoneAuthCredential credential) async {
          add(_OnPhoneAuthVerificationCompleteEvent(credential: credential));
        },
        codeSent: (String verificationId, int? resendToken) {
          add(
            _OnPhoneOtpSent(
              verificationId: verificationId,
              token: resendToken,
            ),
          );
        },
        verificationFailed: (FirebaseAuthException e) {
          add(_OnPhoneAuthErrorEvent(error: e.code));
        },
        codeAutoRetrievalTimeout: (String verificationId) {},
      );
    } catch (e) {
      emit(PhoneAuthError(error: e.toString()));
    }
  }

  Future<void> _onVerifyOtp(
    _VerifySentOtp event,
    Emitter<PhoneAuthState> emit,
  ) async {
    try {
      emit(const LoadingPhoneAuthState());

      final credential = phoneAuthService.getCredential(
        verificationId: event.verificationId,
        smsCode: event.otpCode,
      );

      add(_OnPhoneAuthVerificationCompleteEvent(credential: credential));
    } catch (e) {
      emit(PhoneAuthError(error: e.toString()));
    }
  }

  Future<void> _loginWithCredential(
    _OnPhoneAuthVerificationCompleteEvent event,
    Emitter<PhoneAuthState> emit,
  ) async {
    try {
      await phoneAuthService.signInWithCredential(credential: event.credential);

      if (FirebaseAuth.instance.currentUser != null) {
        emit(const PhoneAuthVerifiedState());
      }
    } on FirebaseAuthException catch (e) {
      emit(PhoneAuthError(error: e.toString()));
    } catch (e) {
      emit(PhoneAuthError(error: e.toString()));
    }
  }
}
