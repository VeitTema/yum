// ignore_for_file: inference_failure_on_function_return_type

import 'package:firebase_auth/firebase_auth.dart';

abstract class PhoneAuthService {
  const PhoneAuthService();

  bool get isUserAuth;

  Future<void> verifyPhone({
    required String phoneNumber,
    required Function(PhoneAuthCredential) verificationCompleted,
    required Function(FirebaseAuthException) verificationFailed,
    required Function(String, int?) codeSent,
    required Function(String) codeAutoRetrievalTimeout,
  });
  Future<void> signInWithCredential({
    required AuthCredential credential,
  });

  PhoneAuthCredential getCredential({
    required String verificationId,
    required String smsCode,
  });
}
