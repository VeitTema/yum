class UserModel {
  const UserModel({
    required this.phone,
    required this.id,
  });

  UserModel.fromJson(
    Map<String, dynamic> map,
  )   : phone = map['phone'] as String,
        id = map['id'] as String;

  Map<String, dynamic> toJson() {
    return {
      'phone': phone,
      'id': id,
    };
  }

  UserModel copyWith({
    String? phone,
    String? id,
  }) {
    return UserModel(
      phone: phone ?? this.phone,
      id: id ?? this.id,
    );
  }

  final String phone;
  final String id;
}
