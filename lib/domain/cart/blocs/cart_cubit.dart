import 'package:bloc/bloc.dart';
import 'package:yum/app/helpers/async_data.dart';
import 'package:yum/domain/cart/models/cart_product.dart';
import 'package:yum/domain/cart/services/cart_service.dart';
import 'package:yum/domain/menu/models/product.dart';

typedef CartState = AsyncData<List<CartProduct>, Object>;

class CartCubit extends Cubit<CartState> {
  CartCubit({required this.cartService}) : super(const AsyncData.initial([]));

  final CartService cartService;

  Future<void> getCart() async {
    emit(state.inLoading());
    try {
      final cartProducts = await cartService.getAllProducts();

      emit(CartState.success(cartProducts));
    } on Object catch (_) {
      emit(state.inFailure());
      rethrow;
    }
  }

  Future<void> addToCart(Product product) async {
    try {
      final cartProduct = await cartService.findProductByTitle(product.title);
      if (cartProduct != null) {
        await cartService
            .update(cartProduct.copyWith(count: cartProduct.count + 1));
        final cartProducts = await cartService.getAllProducts();

        emit(CartState.success(cartProducts));
      } else {
        await cartService.insert(product);
        final cartProducts = await cartService.getAllProducts();
        emit(CartState.success(cartProducts));
      }
    } on Object catch (_) {
      emit(state.inFailure());
      rethrow;
    }
  }

  Future<void> deleteFromCart(Product product) async {
    try {
      final cartProduct = await cartService.findProductByTitle(product.title);
      if (cartProduct != null && cartProduct.count > 1) {
        await cartService
            .update(cartProduct.copyWith(count: cartProduct.count - 1));
        final cartProducts = await cartService.getAllProducts();

        emit(CartState.success(cartProducts));
      } else if (cartProduct != null && cartProduct.count <= 1) {
        await cartService.delete(cartProduct.id);
        final cartProducts = await cartService.getAllProducts();
        emit(CartState.success(cartProducts));
      }
    } on Object catch (_) {
      emit(state.inFailure());
      rethrow;
    }
  }
}
