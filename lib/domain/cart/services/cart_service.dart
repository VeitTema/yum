import 'package:yum/domain/cart/models/cart_product.dart';
import 'package:yum/domain/menu/models/product.dart';

abstract class CartService {
  const CartService();

  Future<List<CartProduct>> getAllProducts();

  Future<CartProduct?> findProductByTitle(String title);

  Future<int> insert(Product product);

  Future<int> update(CartProduct cartProduct);

  Future<void> delete(int id);
}
