import 'package:yum/domain/menu/models/product.dart';

class CartProduct extends Product {
  CartProduct({
    required this.id,
    required this.count,
    required super.title,
    required super.description,
    required super.cost,
    required super.imageUrl,
  });

  factory CartProduct.fromMap(Map<String, dynamic> map) {
    return CartProduct(
      id: map['id'] as int,
      count: map['count'] as int,
      title: map['title'] as String,
      description: map['description'] as String,
      cost: map['cost'] as int,
      imageUrl: map['imageUrl'] as String,
    );
  }

  final int id;
  final int count;

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'count': count,
      'title': title,
      'description': description,
      'cost': cost,
      'imageUrl': imageUrl,
    };
  }

  @override
  CartProduct copyWith({
    int? id,
    int? count,
    String? title,
    String? description,
    int? cost,
    String? imageUrl,
  }) =>
      CartProduct(
        id: id ?? this.id,
        count: count ?? this.count,
        title: title ?? this.title,
        description: description ?? this.description,
        cost: cost ?? this.cost,
        imageUrl: imageUrl ?? this.imageUrl,
      );
}
