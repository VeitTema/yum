import 'package:yum/domain/menu/models/product.dart';

class Category {
  Category({required this.categoryName, required this.products});

  factory Category.fromMap(Map<String, dynamic> map) {
    return Category(
      categoryName: map['categoryName'] as String,
      products: (map['products'] as List)
          .map((product) => Product.fromMap(product as Map<String, dynamic>))
          .toList(),
    );
  }

  final String categoryName;
  final List<Product> products;
}
