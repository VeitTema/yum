class News {
  const News({
    required this.label,
    required this.imageUrl,
    required this.stories,
  });

  factory News.fromMap(Map<String, dynamic> map) {
    return News(
      label: map['label'] as String,
      imageUrl: map['imageUrl'] as String,
      stories:
          (map['stories'] as List).map((story) => story as String).toList(),
    );
  }

  final String label;
  final String imageUrl;
  final List<String> stories;

  static const newsList = [
    {
      'label': '2 pizzas for the price of one',
      'imageUrl':
          'https://img.freepik.com/premium-photo/classic-italian-pepperoni-pizza-on-black-background-top-view-high-quality-photo_275899-626.jpg',
      'stories': [
        'https://images.pexels.com/photos/315755/pexels-photo-315755.jpeg?auto=compress&cs=tinysrgb&w=1600',
        'https://images.pexels.com/photos/7676355/pexels-photo-7676355.jpeg?auto=compress&cs=tinysrgb&w=1600',
        'https://images.pexels.com/photos/7142954/pexels-photo-7142954.jpeg?auto=compress&cs=tinysrgb&w=1600',
        'https://images.pexels.com/photos/3581878/pexels-photo-3581878.jpeg?auto=compress&cs=tinysrgb&w=1600',
      ],
    },
    {
      'label': ' The best kebab in town',
      'imageUrl':
          'https://kurkumashaurma.by/assets/images/products/138/tureczkaya-1200-dsc-5911.jpg',
      'stories': [
        'https://images.pexels.com/photos/7426866/pexels-photo-7426866.jpeg?auto=compress&cs=tinysrgb&w=1600',
      ],
    },
    {
      'label': 'New feelings with Coca-Cola',
      'imageUrl':
          'https://www.crushpixel.com/big-static18/preview4/kuala-lumpur-malaysia-october-19-2708641.jpg',
      'stories': [
        'https://images.pexels.com/photos/2983100/pexels-photo-2983100.jpeg?auto=compress&cs=tinysrgb&w=1600',
        'https://images.pexels.com/photos/2983103/pexels-photo-2983103.jpeg?auto=compress&cs=tinysrgb&w=1600'
      ],
    },
    {
      'label': 'Weekend specialty bakery',
      'imageUrl':
          'https://st.focusedcollection.com/16017410/i/650/focused_183511894-stock-photo-buns-pomegranate-filling-black-background.jpg',
      'stories': [
        'https://images.pexels.com/photos/8356208/pexels-photo-8356208.jpeg?auto=compress&cs=tinysrgb&w=1600',
        'https://images.pexels.com/photos/2135/food-france-morning-breakfast.jpg?auto=compress&cs=tinysrgb&w=1600',
        'https://images.pexels.com/photos/5945568/pexels-photo-5945568.jpeg?auto=compress&cs=tinysrgb&w=1600'
      ],
    },
    {
      'label': 'Hot soups for a quick snack',
      'imageUrl':
          'https://img.freepik.com/premium-photo/shrimp-soup-in-a-white-cup-on-a-black-background-isolated_508835-6597.jpg',
      'stories': [
        'https://images.pexels.com/photos/1907244/pexels-photo-1907244.jpeg?auto=compress&cs=tinysrgb&w=1600',
        'https://images.pexels.com/photos/5409015/pexels-photo-5409015.jpeg?auto=compress&cs=tinysrgb&w=1600'
      ],
    },
  ];
}
