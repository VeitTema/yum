import 'package:yum/domain/menu/models/category.dart';

class ProductMenu {
  ProductMenu({
    required this.categories,
  });

  factory ProductMenu.fromMap(Map<String, dynamic> map) {
    return ProductMenu(
      categories: (map['menu'] as List)
          .map((category) => Category.fromMap(category as Map<String, dynamic>))
          .toList(),
    );
  }

  final List<Category> categories;

  static final menu = {
    'menu': [
      {
        'categoryName': 'Pizza',
        'products': [
          {
            'title': 'Margherita',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2017/01/03/11/33/pizza-1949183_960_720.jpg'
          },
          {
            'title': 'Marinara',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2017/02/15/10/57/pizza-2068272__340.jpg'
          },
          {
            'title': 'Carbonara',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2018/12/12/13/03/pizza-3870778__340.jpg'
          },
          {
            'title': 'Siciliana',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2016/05/05/09/19/pizza-1373508__340.jpg'
          },
          {
            'title': 'Napoletana',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://img.freepik.com/premium-photo/classic-italian-pepperoni-pizza-on-black-background-top-view-high-quality-photo_275899-626.jpg'
          },
          {
            'title': 'Romana',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2018/04/11/03/13/food-3309418__340.jpg'
          },
          {
            'title': 'Sarda',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2016/06/08/00/03/pizza-1442946__340.jpg'
          },
          {
            'title': 'Tonno',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2015/12/07/22/27/pizza-1081534__340.jpg'
          },
        ],
      },
      {
        'categoryName': 'Kebab',
        'products': [
          {
            'title': 'Shawarma',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2017/07/14/23/25/kebab-2505236__340.jpg'
          },
          {
            'title': 'Doner Kebab',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2017/02/09/14/09/kebab-2052498__340.jpg'
          },
          {
            'title': 'Durum',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2016/02/13/16/54/food-1198204__340.jpg'
          },
          {
            'title': 'Gyros',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://media.istockphoto.com/id/1349600163/photo/breakfast-tortilla-wrap-with-omelet-beans-and-vegetables.jpg?b=1&s=170667a&w=0&k=20&c=e_UWjMVobYxVtPpYynmjr_SxGylkRA7f2qSYw4qGGOA='
          },
          {
            'title': 'Brtuch',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://media.istockphoto.com/id/689572418/photo/nice-vegetarian-burrito-over-black-table-on-wooden-board.jpg?b=1&s=170667a&w=0&k=20&c=NvrxBX4051EBkUFmX7ndnfgDfg__cwzbuMtKOSqiPSI='
          },
          {
            'title': 'Burrito',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://media.istockphoto.com/id/1352567243/photo/healthy-homemade-carnitas-pork-burrito.jpg?b=1&s=170667a&w=0&k=20&c=qIpElT2IywCOt3n1OtcJ2Or6VC6OjLGhBVuqxDh8wVE='
          },
        ],
      },
      {
        'categoryName': 'Drink',
        'products': [
          {
            'title': 'Lemonade',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2018/07/12/21/45/drink-3534412__340.jpg'
          },
          {
            'title': 'Fruit juice',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2017/04/23/09/44/smoothies-2253423__340.jpg'
          },
          {
            'title': 'Mineral water',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2017/08/05/01/33/bottle-2582012__340.jpg'
          },
          {
            'title': 'Herbal tea',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2018/09/13/03/47/tea-3673714__340.jpg'
          },
          {
            'title': 'Coffee',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2016/04/26/16/58/coffe-1354786__340.jpg'
          },
          {
            'title': 'Coke',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2014/09/26/19/51/drink-462776__340.jpg'
          },
        ],
      },
      {
        'categoryName': 'Soup',
        'products': [
          {
            'title': 'Red borsch',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2015/02/03/16/31/soup-622737__340.jpg'
          },
          {
            'title': 'Cabbasge soup',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2014/12/16/23/45/soup-570922__340.jpg'
          },
          {
            'title': 'Cheese cream soup',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2016/11/01/11/37/soup-1787997__340.jpg'
          },
          {
            'title': 'Homemade noodles with chicken',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2019/02/22/23/50/goulash-4014661__340.jpg'
          },
          {
            'title': 'Kharcho',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2018/10/25/19/15/goulash-3773134__340.jpg'
          },
          {
            'title': 'Solynka',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2017/03/17/17/33/potato-soup-2152254__340.jpg'
          },
          {
            'title': 'Milk soup',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2021/12/27/04/59/egg-6896278__340.jpg'
          },
        ],
      },
      {
        'categoryName': 'Sushi',
        'products': [
          {
            'title': 'Koji',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2018/08/03/08/33/food-3581341__340.jpg'
          },
          {
            'title': 'Guncan',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2017/10/16/09/00/sushi-2856544__340.jpg'
          },
          {
            'title': 'Yaki',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2016/05/21/14/09/food-1406883__340.jpg'
          },
          {
            'title': 'Kappa',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2015/03/23/10/27/sushi-685912__340.jpg'
          },
          {
            'title': 'Nigiri',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2017/01/30/08/26/sushi-2020287__340.jpg'
          },
        ],
      },
      {
        'categoryName': 'Bakery',
        'products': [
          {
            'title': 'Cheesecake',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2016/08/08/16/20/cheesecake-1578694__340.jpg'
          },
          {
            'title': 'Almond cookie',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2017/07/28/14/23/macarons-2548810__340.jpg'
          },
          {
            'title': 'Banana split',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2017/08/31/15/49/banana-split-2701128__340.jpg'
          },
          {
            'title': 'Cherry pie',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2018/02/18/19/07/cake-3163117__340.jpg'
          },
          {
            'title': 'Cupcake',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2019/02/18/14/29/food-4004585_960_720.jpg'
          },
          {
            'title': 'Eclair',
            'description': 'description',
            'cost': 10,
            'imageUrl':
                'https://cdn.pixabay.com/photo/2018/05/01/18/21/eclair-3366430__340.jpg'
          },
        ],
      },
    ]
  };
}
