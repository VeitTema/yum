class Product {
  Product({
    required this.title,
    required this.description,
    required this.cost,
    required this.imageUrl,
  });

  factory Product.fromMap(Map<String, dynamic> map) {
    return Product(
      title: map['title'] as String,
      description: map['description'] as String,
      cost: map['cost'] as int,
      imageUrl: map['imageUrl'] as String,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'description': description,
      'cost': cost,
      'imageUrl': imageUrl,
    };
  }

  Product copyWith({
    String? title,
    String? description,
    int? cost,
    String? imageUrl,
  }) =>
      Product(
        title: title ?? this.title,
        description: description ?? this.description,
        cost: cost ?? this.cost,
        imageUrl: imageUrl ?? this.imageUrl,
      );

  final String title;
  final String description;
  final int cost;
  final String imageUrl;
}
