import 'package:yum/app/helpers/result.dart';
import 'package:yum/domain/menu/models/product_menu.dart';

abstract class MenuService {
  const MenuService();

  Future<Result<ErrorLoadingMenu, ProductMenu>> fetchProductMenu();
}

enum ErrorLoadingMenu { unknown }
