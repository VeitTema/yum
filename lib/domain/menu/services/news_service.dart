import 'package:yum/app/helpers/result.dart';
import 'package:yum/domain/menu/models/news.dart';

abstract class NewsService {
  const NewsService();

  Future<Result<ErrorLoadingNews, List<News>>> fetchNews();
}

enum ErrorLoadingNews { unknown }
