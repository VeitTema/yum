import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yum/app/helpers/async_data.dart';

import 'package:yum/domain/menu/models/product_menu.dart';
import 'package:yum/domain/menu/services/menu_service.dart';

typedef MenuState = AsyncData<ProductMenu, Object>;

class MenuCubit extends Cubit<MenuState> {
  MenuCubit({required this.menuService})
      : super(AsyncData.initial(ProductMenu(categories: [])));

  final MenuService menuService;

  Future<void> fetchMenu() async {
    try {
      emit(state.inLoading());

      final result = await menuService.fetchProductMenu();
      emit(
        result.when(
          error: (e) => state.inFailure(),
          value: (pointList) {
            return MenuState.success(pointList);
          },
        ),
      );
    } on Object catch (_) {
      emit(state.inFailure());
      rethrow;
    }
  }
}
