import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yum/app/helpers/async_data.dart';
import 'package:yum/domain/menu/models/news.dart';
import 'package:yum/domain/menu/services/news_service.dart';

typedef NewsState = AsyncData<List<News>, Object>;

class NewsCubit extends Cubit<NewsState> {
  NewsCubit({required this.newsService})
      : super(const AsyncData.initial(<News>[]));

  final NewsService newsService;

  Future<void> fetchNews() async {
    try {
      emit(state.inLoading());
      final result = await newsService.fetchNews();

      emit(
        result.when(
          error: (e) => state.inFailure(),
          value: (newsList) {
            return NewsState.success(
              newsList,
            );
          },
        ),
      );
    } on Object catch (_) {
      emit(state.inFailure());
      rethrow;
    }
  }
}
