import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:yum/app/app.dart';
import 'package:yum/bootstrap.config.dart';
import 'package:yum/data/services/fake_map_service.dart';
import 'package:yum/data/services/fake_menu_service.dart';
import 'package:yum/data/services/fake_news_service.dart';
import 'package:yum/data/services/sqlite_db_service.dart';
import 'package:yum/domain/cart/blocs/cart_cubit.dart';
import 'package:yum/domain/map/blocs/map_cubit.dart';
import 'package:yum/domain/menu/blocs/menu_cubit.dart';
import 'package:yum/domain/menu/blocs/news_cubit.dart';

Future<void> bootstrap() async {
  await configureDependencies();

  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
              CartCubit(cartService: SQLiteDbService.db)..getCart(),
        ),
        BlocProvider(
          create: (context) =>
              MenuCubit(menuService: const FakeMenuService())..fetchMenu(),
        ),
        BlocProvider(
          create: (context) =>
              NewsCubit(newsService: const FakeNewsService())..fetchNews(),
        ),
        BlocProvider(
          create: (context) =>
              MapCubit(mapService: const FakeMapService())..initial(),
        ),
      ],
      child: const App(),
    ),
  );
}

@InjectableInit()
Future<void> configureDependencies() async {
  final getIt = GetIt.instance;
  await _configureManualDeps(getIt);
  $initGetIt(getIt);
}

Future<void> _configureManualDeps(GetIt getIt) async {}
