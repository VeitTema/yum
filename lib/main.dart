import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:yum/bootstrap.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();

  await bootstrap();
}
