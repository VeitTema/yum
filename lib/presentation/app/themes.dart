import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:yum/presentation/app/brightness.dart';
import 'package:yum/presentation/app/colors.dart';
import 'package:yum/presentation/app/screen_size_helper.dart';

class MaterialTheme {
  static const _fontFamily = 'Poppins';

  static ThemeData from(AppColorsData colors) {
    return ThemeData(
      cupertinoOverrideTheme: CupertinoThemeData(primaryColor: colors.text),
      brightness: Brightness.light,
      fontFamily: _fontFamily,
      visualDensity: VisualDensity.adaptivePlatformDensity,
      dividerTheme: const DividerThemeData(
        space: 1,
      ),
      canvasColor: colors.background,
      backgroundColor: colors.background,
      dialogBackgroundColor: colors.background,
      scaffoldBackgroundColor: colors.background,
      bottomAppBarColor: colors.appBarColor,

      errorColor: colors.error,
      disabledColor: colors.disabled,
      //
      colorScheme: ColorScheme.fromSeed(
        seedColor: colors.primary,
        primary: colors.primary,
        onPrimary: colors.onPrimary,
        secondary: colors.primary,
        onSecondary: colors.onPrimary,
        error: colors.error,
        onError: colors.onError,
        surface: colors.surface,
        onSurface: colors.onSurface,
        background: colors.background,
        onBackground: colors.onBackground,
      ),

      primaryColor: colors.primary,

      shadowColor: colors.background[20],

      dividerColor: colors.background[20],

      textTheme: TextTheme(
        headline1: TextStyle(
          height: 1.17,
          fontSize: 24.fo,
          letterSpacing: 0.33,
          fontWeight: FontWeight.w700,
          color: colors.text,
        ),
        // default TextField
        headline3: TextStyle(
          height: 1.25,
          fontSize: 16.fo,
          letterSpacing: -0.32,
          fontWeight: FontWeight.w400,
          color: colors.text,
        ),
        headline4: TextStyle(
          height: 1.33,
          fontSize: 15.fo,
          letterSpacing: -0.24,
          fontWeight: FontWeight.w500,
          color: colors.text,
        ),
        bodyText2: TextStyle(
          fontSize: 14.fo,
        ),
        subtitle1: TextStyle(
          height: 1.33,
          fontSize: 15.fo,
          letterSpacing: -0.24,
          fontWeight: FontWeight.w400,
          color: colors.text[200],
        ),
        subtitle2: TextStyle(
          height: 1.29,
          fontSize: 14.fo,
          letterSpacing: -0.15,
          fontWeight: FontWeight.w400,
          color: colors.text,
        ),
        caption: TextStyle(
          height: 1.23,
          fontSize: 13.fo,
          letterSpacing: -0.08,
          fontWeight: FontWeight.w400,
          color: colors.text[300],
        ),
        button: TextStyle(
          height: 1.17,
          fontSize: 16.fo,
          letterSpacing: 0.33,
          fontWeight: FontWeight.w600,
          color: colors.text[50],
        ),
      ),

      tabBarTheme: TabBarTheme(
        indicator: UnderlineTabIndicator(
          borderSide: BorderSide(color: colors.primary, width: 2.si),
          insets: EdgeInsets.symmetric(horizontal: 16.si),
        ),
        labelColor: colors.text,
        unselectedLabelColor: colors.text[200],
        unselectedLabelStyle: TextStyle(
          height: 1.25,
          fontSize: 16.fo,
          letterSpacing: -0.32,
          fontWeight: FontWeight.w500,
        ),
        labelStyle: TextStyle(
          height: 1.25,
          fontSize: 16.fo,
          letterSpacing: -0.32,
          fontWeight: FontWeight.w500,
        ),
      ),

      iconTheme: IconThemeData(
        size: 16.si,
      ),

      radioTheme: RadioThemeData(
        fillColor: MaterialStateProperty.all(colors.primary),
      ),

      snackBarTheme: SnackBarThemeData(
        elevation: 32,
        behavior: SnackBarBehavior.floating,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: colors.primary, width: 2),
          borderRadius: const BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        backgroundColor: colors.background,
        contentTextStyle: TextStyle(
          height: 1.33,
          fontSize: 16.fo,
          color: colors.primary,
          letterSpacing: -0.24,
          fontFamily: _fontFamily,
          fontWeight: FontWeight.w400,
        ),
      ),

      dialogTheme: DialogTheme(
        shape: BeveledRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(
              15.si,
            ),
          ),
        ),
        elevation: 0,
        titleTextStyle: TextStyle(
          fontSize: 17.fo,
          fontWeight: FontWeight.w500,
          color: colors.text,
        ),
        contentTextStyle: TextStyle(
          fontSize: 14.fo,
          color: colors.text,
        ),
      ),

      listTileTheme: ListTileThemeData(
        contentPadding: EdgeInsets.symmetric(
          horizontal: 16.si,
        ),
        minVerticalPadding: 10.si,
      ),

      appBarTheme: AppBarTheme(
        elevation: 0,
        titleSpacing: 0,
        toolbarHeight: kToolbarHeight.si,
        centerTitle: true,
        color: colors.appBarColor,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarBrightness: colors.brightness.when(
            dark: () => Brightness.dark,
            light: () => Brightness.light,
          ),
          statusBarColor: Colors.transparent,
        ),
        iconTheme: IconThemeData(
          size: 25.si,
          color: colors.text,
        ),
        titleTextStyle: TextStyle(
          height: 1.17,
          fontSize: 24.fo,
          letterSpacing: 0.33,
          fontWeight: FontWeight.w700,
          color: colors.text,
        ),
      ),
      bottomNavigationBarTheme: BottomNavigationBarThemeData(
        selectedItemColor: colors.primary,
        unselectedItemColor: colors.text[100],
        backgroundColor: Colors.transparent,
        elevation: 0,
        selectedLabelStyle: TextStyle(
          height: 1.27,
          fontSize: 11.fo,
          fontFamily: _fontFamily,
          fontWeight: FontWeight.w400,
          letterSpacing: 0.06,
        ),
        unselectedLabelStyle: TextStyle(
          height: 1.27,
          fontSize: 11.fo,
          fontFamily: _fontFamily,
          fontWeight: FontWeight.w400,
          letterSpacing: 0.06,
        ),
        selectedIconTheme: const IconThemeData(
          size: 28,
        ),
        unselectedIconTheme: const IconThemeData(
          size: 28,
        ),
        showUnselectedLabels: true,
      ),

      switchTheme: SwitchThemeData(
        thumbColor: MaterialStateProperty.all(colors.primary),
        trackColor: MaterialStateProperty.all(colors.primary.withOpacity(.4)),
      ),

      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          elevation: 0,
          foregroundColor: colors.onPrimary,
          textStyle: TextStyle(
            height: 1.3,
            fontSize: 17.fo,
            letterSpacing: -0.41,
            fontFamily: _fontFamily,
            fontWeight: FontWeight.w500,
          ),
          primary: colors.primary,
          shadowColor: Colors.transparent,
          padding: EdgeInsets.symmetric(
            vertical: 10.si,
            horizontal: 16.si,
          ).copyWith(bottom: 12.si),
        ).copyWith(
          backgroundColor: MaterialStateProperty.resolveWith((states) {
            if (states.contains(MaterialState.disabled)) {
              return colors.primary[100];
            }
            return colors.primary;
          }),
          foregroundColor: MaterialStateProperty.all(colors.onPrimary),
        ),
      ),

      textButtonTheme: TextButtonThemeData(
        style: TextButton.styleFrom(
          backgroundColor: colors.background[40],
          foregroundColor: colors.accent,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          textStyle: TextStyle(
            height: 1.29,
            fontSize: 14.fo,
            fontFamily: _fontFamily,
            fontWeight: FontWeight.w500,
            letterSpacing: -0.15,
          ),
          padding: EdgeInsets.symmetric(vertical: 8.si, horizontal: 16.si),
        ),
      ),

      inputDecorationTheme: InputDecorationTheme(
        contentPadding: const EdgeInsets.all(16),
        labelStyle: TextStyle(
          fontSize: 16.fo,
          height: 1.25,
          letterSpacing: -0.32,
          color: colors.text[200],
          fontFamily: _fontFamily,
          fontWeight: FontWeight.w400,
        ),
        suffixStyle: TextStyle(
          color: colors.text[200],
        ),
        prefixStyle: TextStyle(
          color: colors.text[200],
        ),
        fillColor: colors.background[30],
        hintStyle: TextStyle(
          fontSize: 16.fo,
          height: 1.25,
          letterSpacing: -0.32,
          color: colors.text[200],
          fontFamily: _fontFamily,
          fontWeight: FontWeight.w400,
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          borderSide: BorderSide(
            color: colors.error,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          borderSide: BorderSide(
            color: colors.enabledBorder,
            width: 0.5.si,
          ),
        ),
        border: OutlineInputBorder(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          borderSide: BorderSide(
            color: colors.enabledBorder,
            width: 0.5.si,
          ),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          borderSide: BorderSide(
            color: colors.enabledBorder,
            width: 0.5.si,
          ),
        ),
        disabledBorder: OutlineInputBorder(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          borderSide: BorderSide(
            color: colors.enabledBorder,
            width: 0.5.si,
          ),
        ),
      ),
    );
  }
}
