import 'package:flutter/widgets.dart';
import 'package:yum/presentation/app/screen_size_helper.dart';

extension SizeHelperExtension on EdgeInsets {
  EdgeInsets get si => copyWith(
        bottom: bottom.si,
        left: left.si,
        right: right.si,
        top: top.si,
      );
}
