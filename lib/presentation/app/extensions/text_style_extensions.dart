import 'package:flutter/widgets.dart';
import 'package:yum/presentation/app/screen_size_helper.dart';

extension SizeHelperExtension on TextStyle {
  TextStyle get fo => copyWith(
        fontSize: fontSize?.fo,
      );
}
