import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:yum/presentation/app/brightness.dart';

/// Defines color palette
abstract class AppColorsData with EquatableMixin {
  @literal
  const AppColorsData();

  /// Creates light color scheme
  @literal
  const factory AppColorsData.light() = _AppColorsDataLight;

  factory AppColorsData.fromBrightness(AppBrightness brightness) {
    return brightness.when(
      dark: () => throw UnimplementedError('Dark theme nor supported yet'),
      light: () => const _AppColorsDataLight(),
    );
  }

  /// The brightness of color palette
  AppBrightness get brightness;

  /// AppBar color
  Color get appBarColor;

  /// Text colors set
  ColorSwatch<int> get text;

  /// Primary colors set
  ColorSwatch<int> get primary;

  Color get onPrimary;

  /// Background colors set
  ColorSwatch<int> get background;

  Color get onBackground;

  /// Surface colors set
  ColorSwatch<int> get surface;

  Color get onSurface;

  /// Color for errors
  Color get error;

  /// Color for warning
  Color get warning;

  /// Success
  Color get success;

  /// Accent
  Color get accent;

  /// Disabled
  Color get disabled;

  Color get onError;

  Color get enabledBorder;

  Color get defaultLabelBackgroundColor;

  @override
  List<Object?> get props => [brightness];
}

class _AppColorsDataLight extends AppColorsData {
  const _AppColorsDataLight();

  @override
  AppBrightness get brightness => AppBrightness.light;

  @override
  Color get appBarColor => const Color(0xFFFFFFFF);

  @override
  ColorSwatch<int> get text {
    return const ColorSwatch(
      0xFF000000,
      {
        50: Color(0xFFFFFFFF),
        100: Color(0xFFB3B3B3),
        200: Color(0xFF818C99),
        300: Color(0xFF4D4D4D),
        400: Color(0xFF262626),
        500: Color(0xFF000000),
        600: Color(0xFF000000),
        700: Color(0xFF000000),
        800: Color(0xFF000000),
        900: Color(0xFF000000),
      },
    );
  }

  @override
  ColorSwatch<int> get primary {
    return const ColorSwatch(
      0xFFFF6600,
      {
        50: Color(0xFFFF6600),
        100: Color(0xFFFF6600),
        200: Color(0xFFFF6600),
        300: Color(0xFFFF6600),
        400: Color(0xFFFF6600),
        500: Color(0xFFFF6600),
        600: Color(0xFFFF6600),
        700: Color(0xFFFF6600),
        800: Color(0xFFFF6600),
        900: Color(0xFFFF6600),
      },
    );
  }

//
  @override
  Color get onPrimary => const Color(0xFFFFFFFF);
//
  @override
  ColorSwatch<int> get background {
    return const ColorSwatch(
      0xFFFFFFFF,
      {
        0: Color(0xFFF2F2F2),
        5: Color(0xFFD9D9D9),
        10: Color(0xFFBFBFBF),
        20: Color(0xFFF1F1F1),
        30: Color(0xFFF5F5F5),
        40: Color(0xFFF2F4F5),
        50: Color(0xFFD9D9D9),
        56: Color(0xFF595959),
        75: Color(0xFF404040),
        100: Color(0xFF262626),
      },
    );
  }

  @override
  Color get onBackground => background[75]!;

  @override
  ColorSwatch<int> get surface => background;

  @override
  Color get onSurface => onBackground;

  @override
  Color get error => const Color(0xFFE64646);

  @override
  Color get onError => const Color(0xFFFFFFFF);

  @override
  Color get success => const Color(0xFF4BB34B);

  @override
  Color get accent => const Color(0xFF2688EB);

  @override
  Color get warning => const Color(0xFFFFA000);

  @override
  Color get disabled => const Color(0xFF99A2AD);

  @override
  Color get enabledBorder => const Color(0x1F000000);

  @override
  Color get defaultLabelBackgroundColor => const Color(0x0D001C3D);
}

class AppColors extends StatelessWidget {
  const AppColors({
    required this.child,
    required this.data,
    super.key,
  });

  @protected
  final Widget child;

  @protected
  final AppColorsData data;

  static AppColorsData of(BuildContext context, {bool watch = true}) {
    final getInheritedElement = context.getElementForInheritedWidgetOfExactType;
    final inheritedColors = getInheritedElement<InheritedColors>();

    if (inheritedColors == null) {
      throw FlutterError(
        'AppColors.of(context) called with a context that does not contain a '
        'AppColorsData',
      );
    }

    if (watch) {
      context.dependOnInheritedElement(inheritedColors);
    }

    return (inheritedColors.widget as InheritedColors).colors.data;
  }

  @override
  Widget build(BuildContext context) {
    return InheritedColors(
      colors: this,
      child: child,
    );
  }
}

@protected
class InheritedColors extends InheritedWidget {
  @literal
  const InheritedColors({
    required super.child,
    required this.colors,
    super.key,
  });
  @protected
  final AppColors colors;

  @override
  bool updateShouldNotify(InheritedColors oldWidget) {
    return colors != oldWidget.colors;
  }
}
