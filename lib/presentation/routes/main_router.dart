import 'package:auto_route/auto_route.dart';
import 'package:auto_route/empty_router_widgets.dart';
import 'package:flutter/material.dart';
import 'package:yum/app/scopes/authentication_scope.dart';
import 'package:yum/domain/menu/models/news.dart';
import 'package:yum/domain/menu/models/product.dart';
import 'package:yum/presentation/screens/auth/enter_otp_code_screen.dart';
import 'package:yum/presentation/screens/auth/enter_phone_number_screen.dart';
import 'package:yum/presentation/screens/cart/cart_screen.dart';
import 'package:yum/presentation/screens/home/home_screen.dart';
import 'package:yum/presentation/screens/map/map_screen.dart';
import 'package:yum/presentation/screens/menu/menu_screen.dart';
import 'package:yum/presentation/screens/news/story_screen.dart';
import 'package:yum/presentation/screens/product/product_screen.dart';
import 'package:yum/presentation/screens/profile/profile_screen.dart';

part 'main_router.gr.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Screen|Scope,Route',
  routes: <AutoRoute>[
    AutoRoute<void>(
      path: 'home',
      page: EmptyRouterPage,
      name: 'HomeFlow',
      children: [
        AutoRoute<void>(
          initial: true,
          page: HomeScreen,
          children: [
            AutoRoute<void>(
              path: 'menu',
              page: MenuScreen,
            ),
            AutoRoute<void>(
              path: 'profile',
              page: ProfileScreen,
            ),
            AutoRoute<void>(
              path: 'cart',
              page: CartScreen,
            ),
          ],
        ),
        AutoRoute<void>(
          path: 'product',
          page: ProductScreen,
        ),
        AutoRoute<void>(
          path: 'news',
          page: StoryScreen,
        ),
        AutoRoute<void>(
          path: 'map',
          page: MapScreen,
        ),
        AutoRoute<void>(
          path: 'auth',
          page: AuthenticationScope,
          name: 'AuthenticationFlow',
          children: [
            AutoRoute<void>(
              initial: true,
              page: EnterPhoneNumberScreen,
              path: 'enter-phone',
            ),
            AutoRoute<void>(
              path: 'enter-code',
              page: EnterOtpCodeScreen,
            ),
          ],
        ),
      ],
    ),
  ],
)
class MainRouter extends _$MainRouter {}
