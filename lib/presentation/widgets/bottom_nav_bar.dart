import 'package:flutter/material.dart';
import 'package:yum/presentation/app/extensions/text_style_extensions.dart';
import 'package:yum/presentation/app/screen_size_helper.dart';

class BottomNavBar extends StatelessWidget {
  const BottomNavBar({
    super.key,
    required this.currentIndex,
    required this.onTap,
    required this.items,
    this.selectedColor,
    this.unselectedColor,
  });

  final int currentIndex;
  final void Function(int index) onTap;
  final List<BottomNavBarItem> items;

  final Color? selectedColor;
  final Color? unselectedColor;

  static const textStyle = TextStyle(
    height: 1.27,
    fontSize: 11,
    letterSpacing: 0.06,
    fontWeight: FontWeight.w400,
  );

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 5.si, bottom: 1.si),
      child: SafeArea(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: List<Widget>.generate(
            items.length,
            (index) {
              final color = index == currentIndex
                  ? selectedColor ?? Theme.of(context).primaryColor
                  : unselectedColor ?? Theme.of(context).disabledColor;
              return InkWell(
                key: items[index].key,
                borderRadius: BorderRadius.circular(30),
                onTap: () {
                  onTap(index);
                },
                child: Container(
                  padding: EdgeInsets.only(top: 5.si),
                  height: 49.si,
                  width: 93.si,
                  child: Column(
                    children: [
                      Icon(
                        items[index].iconData,
                        size: 22.si,
                        color: color,
                      ),
                      SizedBox(
                        height: 3.si,
                      ),
                      Text(
                        items[index].label,
                        style: textStyle.fo.copyWith(color: color),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

class BottomNavBarItem {
  const BottomNavBarItem({
    required this.iconData,
    required this.label,
    this.key,
  });

  final IconData iconData;
  final String label;
  final GlobalKey? key;
}
