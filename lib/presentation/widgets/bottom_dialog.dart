import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:yum/presentation/app/colors.dart';
import 'package:yum/presentation/app/extensions/text_style_extensions.dart';
import 'package:yum/presentation/app/screen_size_helper.dart';
import 'package:yum/presentation/widgets/button.dart';

class BottomDialog extends StatelessWidget {
  const BottomDialog({
    super.key,
    required this.text,
    required this.icon,
    required this.textButton,
    this.onPressed,
  });

  @protected
  final String text;

  @protected
  final IconData icon;

  @protected
  final String textButton;

  @protected
  final VoidCallback? onPressed;

  static const textStyle = TextStyle(
    height: 1.2,
    fontSize: 20,
    letterSpacing: 0.38,
    fontWeight: FontWeight.w600,
  );

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        height: 232.si,
        padding: EdgeInsets.all(16.si),
        margin: EdgeInsets.fromLTRB(8.si, 0, 8.si, 16.si),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(18),
          color: AppColors.of(context).background,
        ),
        width: double.infinity,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              width: double.infinity,
              height: 76.si,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(width: 24.si),
                  Padding(
                    padding: EdgeInsets.only(bottom: 4.si),
                    child: Icon(
                      icon,
                      size: 56.si,
                      color: AppColors.of(context).primary,
                    ),
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: GestureDetector(
                      onTap: () {
                        context.router.pop();
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          color: AppColors.of(context)
                              .text[200]
                              ?.withOpacity(0.12),
                          shape: BoxShape.circle,
                        ),
                        width: 24.si,
                        height: 24.si,
                        child: Icon(
                          Icons.close,
                          size: 10.si,
                          color: AppColors.of(context).text[200],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Text(
              text,
              textAlign: TextAlign.center,
              style: textStyle.fo.copyWith(
                color: AppColors.of(context).text,
              ),
            ),
            SizedBox(height: 32.si),
            SizedBox(
              height: 44.si,
              child: Button(
                padding: EdgeInsets.zero,
                onPressed: onPressed,
                child: Text(textButton),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
