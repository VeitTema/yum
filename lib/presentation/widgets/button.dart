import 'package:flutter/material.dart';
import 'package:yum/presentation/app/extensions/edge_insets_extensions.dart';
import 'package:yum/presentation/app/screen_size_helper.dart';

class Button extends StatefulWidget {
  const Button({
    super.key,
    this.child,
    this.onPressed,
    this.height = 45,
    this.active = true,
    this.stretch = true,
    this.loading = false,
    this.padding = const EdgeInsets.all(10),
  });

  @protected
  final bool stretch;

  @protected
  final bool loading;

  @protected
  final Widget? child;

  @protected
  final double height;

  @protected
  final EdgeInsets padding;

  @protected
  final VoidCallback? onPressed;

  @protected
  final bool active;

  @override
  State<Button> createState() => _ButtonState();
}

class _ButtonState extends State<Button> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.padding.si,
      child: SizedBox(
        width: widget.stretch ? double.infinity : null,
        height: widget.height.si,
        child: AbsorbPointer(
          absorbing: widget.loading,
          child: ElevatedButton(
            onPressed: widget.active ? widget.onPressed : null,
            child: widget.loading
                ? Padding(
                    padding: EdgeInsets.all(2.si),
                    child: const AspectRatio(
                      aspectRatio: 1,
                      child: CircularProgressIndicator(),
                    ),
                  )
                : widget.child,
          ),
        ),
      ),
    );
  }
}
