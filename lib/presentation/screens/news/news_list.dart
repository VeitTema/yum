import 'package:flutter/material.dart';
import 'package:yum/domain/menu/models/news.dart';
import 'package:yum/presentation/screens/news/news_item.dart';

class NewsList extends StatelessWidget {
  const NewsList({
    super.key,
    required this.newsList,
  });
  final List<News> newsList;

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: List.generate(
            newsList.length,
            (index) => NewsItem(
              news: newsList[index],
            ),
          ),
        ),
      ),
    );
  }
}
