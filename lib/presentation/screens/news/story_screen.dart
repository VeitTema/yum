import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

import 'package:yum/domain/menu/models/news.dart';
import 'package:yum/presentation/app/screen_size_helper.dart';

class StoryScreen extends StatefulWidget {
  const StoryScreen({super.key, required this.news});

  final News news;

  @override
  State<StoryScreen> createState() => _StoryScreenState();
}

class _StoryScreenState extends State<StoryScreen> {
  int currentStoryIndex = 0;

  List<double> percentWatched = [];

  @override
  void initState() {
    super.initState();

    // initially, all stories haven't been watched yet
    for (var i = 0; i < widget.news.stories.length; i++) {
      percentWatched.add(0);
    }

    _startWatching();
  }

  void _startWatching() {
    Timer.periodic(const Duration(milliseconds: 50), (timer) {
      setState(() {
        // only add 0.01 as long as it's below 1
        if (percentWatched[currentStoryIndex] + 0.01 < 1) {
          percentWatched[currentStoryIndex] += 0.01;
        }
        // if adding 0.01 exceeds 1, set percentage to 1 and cancel timer
        else {
          percentWatched[currentStoryIndex] = 1;
          timer.cancel();

          // also go to next story as long as there are more stories to go through
          if (currentStoryIndex < widget.news.stories.length - 1) {
            currentStoryIndex++;
            // restart story timer
            _startWatching();
          }
          // if we are finishing the last story then return to homepage
          else {
            Navigator.pop(context);
          }
        }
      });
    });
  }

  void _onTapDown(TapDownDetails details) {
    final screenWidth = MediaQuery.of(context).size.width;
    final dx = details.globalPosition.dx;

    // user taps on first half of screen
    if (dx < screenWidth / 2) {
      setState(() {
        // as long as this isnt the first story
        if (currentStoryIndex > 0) {
          // set previous and curent story watched percentage back to 0
          percentWatched[currentStoryIndex - 1] = 0;
          percentWatched[currentStoryIndex] = 0;

          // go to previous story
          currentStoryIndex--;
        }
      });
    }
    // user taps on second half of screen
    else {
      setState(() {
        // if there are more stories left
        if (currentStoryIndex < widget.news.stories.length - 1) {
          // finish current story
          percentWatched[currentStoryIndex] = 1;
          // move to next story
          currentStoryIndex++;
        }
        // if user is on the last story, finish this story
        else {
          percentWatched[currentStoryIndex] = 1;
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: _onTapDown,
      child: Scaffold(
        backgroundColor: Colors.blueGrey,
        body: SafeArea(
          child: Center(
            child: Stack(
              children: [
                Container(
                  margin: EdgeInsets.all(10.si),
                  clipBehavior: Clip.antiAlias,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white70),
                  child: AspectRatio(
                    aspectRatio: 10 / 16,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: CachedNetworkImage(
                        imageUrl: widget.news.stories[currentStoryIndex],
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5.si),
                  child: MyStoryBars(
                    percentWatched: percentWatched,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class MyStoryBars extends StatelessWidget {
  const MyStoryBars({super.key, required this.percentWatched});
  final List<double> percentWatched;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10.si, vertical: 20.si),
      child: Row(
        children: List.generate(percentWatched.length, (index) {
          return Expanded(
            child: MyProgressBar(percentWatched: percentWatched[index]),
          );
        }),
      ),
    );
  }
}

class MyProgressBar extends StatelessWidget {
  const MyProgressBar({super.key, required this.percentWatched});
  final double percentWatched;

  @override
  Widget build(BuildContext context) {
    return LinearPercentIndicator(
      padding: EdgeInsets.symmetric(
        horizontal: 2.si,
      ),
      lineHeight: 5.si,
      barRadius: const Radius.circular(20),
      percent: percentWatched,
      progressColor: Theme.of(context).primaryColor,
      backgroundColor: Colors.grey.shade200,
    );
  }
}
