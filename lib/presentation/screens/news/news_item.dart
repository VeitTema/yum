import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:yum/domain/menu/models/news.dart';
import 'package:yum/presentation/app/extensions/text_style_extensions.dart';
import 'package:yum/presentation/app/screen_size_helper.dart';
import 'package:yum/presentation/routes/main_router.dart';

class NewsItem extends StatelessWidget {
  const NewsItem({
    super.key,
    required this.news,
  });
  final News news;

  static const textStyle = TextStyle(
    height: 1.25,
    fontSize: 12,
    letterSpacing: -0.8,
    fontWeight: FontWeight.w600,
  );

  @override
  Widget build(BuildContext context) {
    return InkWell(
      overlayColor: MaterialStateProperty.all(
        Colors.orange,
      ),
      onTap: () {
        context.router.push(NewsRoute(news: news));
      },
      child: SizedBox(
        width: 90.si,
        height: 120.si,
        child: Stack(
          children: [
            Container(
              margin: EdgeInsets.all(5.si),
              width: 90.si,
              height: 120.si,
              decoration: BoxDecoration(
                border:
                    Border.all(color: Theme.of(context).primaryColor, width: 2),
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: CachedNetworkImage(
                  imageUrl: news.imageUrl,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 12.si, vertical: 20.si),
              child: Align(
                alignment: Alignment.bottomLeft,
                child: Text(
                  news.label,
                  style: textStyle.fo.copyWith(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
