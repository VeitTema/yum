import 'package:flutter/material.dart';
import 'package:yum/presentation/app/screen_size_helper.dart';

class ProfileListTile extends StatelessWidget {
  const ProfileListTile({
    super.key,
    required this.text,
    required this.icon,
    this.onPressed,
  });

  @protected
  final String text;

  @protected
  final IconData icon;

  @protected
  final VoidCallback? onPressed;

  static const textStyle = TextStyle(
    height: 1.3,
    fontSize: 17,
    letterSpacing: -0.41,
    fontWeight: FontWeight.w400,
  );

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.si),
        child: SizedBox(
          height: 48.si,
          child: Row(
            children: [
              Icon(
                icon,
                size: 25.si,
                color: Theme.of(context).primaryColor,
              ),
              SizedBox(width: 10.si),
              Expanded(
                child: Text(
                  text,
                  style: Theme.of(context).textTheme.headline3,
                ),
              ),
              Icon(
                Icons.navigate_next_rounded,
                size: 25.si,
                color: Theme.of(context).primaryColor,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
