import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:yum/presentation/app/screen_size_helper.dart';
import 'package:yum/presentation/routes/main_router.dart';
import 'package:yum/presentation/screens/profile/profile_list_tile.dart';
import 'package:yum/presentation/widgets/bottom_dialog.dart';
import 'package:yum/presentation/widgets/button.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<User?>(
      stream: FirebaseAuth.instance.authStateChanges(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          String? name;
          return Scaffold(
            appBar: AppBar(
              centerTitle: false,
              title: Padding(
                padding: EdgeInsets.only(left: 16.si, top: 14.si),
                child: const Text(
                  'Profile',
                ),
              ),
              actions: [
                IconButton(onPressed: () {}, icon: const Icon(Icons.sunny))
              ],
            ),
            body: SafeArea(
              child: SingleChildScrollView(
                child: Center(
                  child: Column(
                    children: [
                      SizedBox(
                        height: 30.si,
                      ),
                      SizedBox.square(
                        dimension: 92.si,
                        child: ClipOval(
                          child: CachedNetworkImage(
                            imageUrl:
                                'https://storage.googleapis.com/cms-storage-bucket/780e0e64d323aad2cdd5.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: 20.si,
                          vertical: 10,
                        ),
                        child: Center(
                          child: Column(
                            children: [
                              Visibility(
                                visible: name != null,
                                child: Text(
                                  '',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1
                                      ?.copyWith(fontSize: 18.fo),
                                ),
                              ),
                              Text(
                                '+49 554 355 67 36',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    ?.copyWith(fontSize: 18.fo),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20.si,
                      ),
                      ProfileListTile(
                        text: 'Edit profile',
                        icon: Icons.edit,
                        onPressed: () {},
                      ),
                      ProfileListTile(
                        text: 'Notifications',
                        icon: Icons.notifications_active_outlined,
                        onPressed: () {},
                      ),
                      ProfileListTile(
                        text: 'Contact us',
                        icon: Icons.phone,
                        onPressed: () {},
                      ),
                      ProfileListTile(
                        text: 'Privacy policy',
                        icon: Icons.privacy_tip_rounded,
                        onPressed: () {},
                      ),
                      ProfileListTile(
                        text: 'Terms and conditions',
                        icon: Icons.contact_support,
                        onPressed: () {},
                      ),
                      SizedBox(
                        height: 30.si,
                      ),
                      ProfileListTile(
                        text: 'Sign out',
                        icon: Icons.logout_rounded,
                        onPressed: () {
                          // ignore: inference_failure_on_function_invocation
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return BottomDialog(
                                text: 'Do you really want to sign out of your'
                                    ' account?',
                                icon: Icons.logout_rounded,
                                textButton: 'Sign out',
                                onPressed: () {
                                  FirebaseAuth.instance.signOut();
                                },
                              );
                            },
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        }
        return Scaffold(
          body: SafeArea(
            child: Center(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.si),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 25.si,
                    ),
                    Text(
                      'Lets get acquainted!',
                      style: Theme.of(context).textTheme.headline1,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10.si,
                    ),
                    Text(
                      'We will give you a birthday gift, \nsave your shipping '
                      'address, and help you \nplace your order quickly',
                      style: Theme.of(context).textTheme.bodyText1,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10.si,
                    ),
                    Button(
                      onPressed: () {
                        context.router.push(const AuthenticationFlow());
                      },
                      child: Text(
                        'ENTER THE PHONE',
                        style: Theme.of(context).textTheme.button,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
