import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yum/domain/auth/blocks/phone_auth_bloc.dart';
import 'package:yum/presentation/app/screen_size_helper.dart';
import 'package:yum/presentation/screens/map/custom_back_button.dart';

class EnterOtpCodeScreen extends StatefulWidget {
  const EnterOtpCodeScreen({super.key, required this.phoneNumber});

  final String phoneNumber;

  @override
  State<EnterOtpCodeScreen> createState() => _EnterOtpCodeScreenState();
}

class _EnterOtpCodeScreenState extends State<EnterOtpCodeScreen> {
  final otpCodeController = TextEditingController();

  bool validOtp(String otpCode) {
    return otpCode.isNotEmpty;
  }

  @override
  void dispose() {
    otpCodeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<PhoneAuthBloc, PhoneAuthState>(
      listener: (context, state) {
        if (state is PhoneAuthVerifiedState) {
          context.router.parent()?.pop();
        }
      },
      builder: (context, state) {
        return Scaffold(
          body: SafeArea(
            child: Column(
              children: [
                const Align(
                  alignment: Alignment.topLeft,
                  child: CustomBackButton(),
                ),
                const Spacer(),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.si),
                  child: Column(
                    children: [
                      Text(
                        'Enter code',
                        style: Theme.of(context).textTheme.headline1,
                      ),
                      SizedBox(
                        height: 15.si,
                      ),
                      Text(
                        'We are sent an SMS with an activation'
                        '\ncode to your phone',
                        style: Theme.of(context).textTheme.bodyText2,
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 10.si,
                      ),
                      Text(
                        widget.phoneNumber,
                        style: Theme.of(context).textTheme.bodySmall,
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 30.si,
                      ),
                      TextField(
                        keyboardType: TextInputType.phone,
                        autofocus: true,
                        decoration: InputDecoration(
                          labelStyle:
                              TextStyle(color: Theme.of(context).primaryColor),
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          labelText: 'Otp code',
                          hintText: 'Enter otp code',
                        ),
                        controller: otpCodeController,
                      ),
                    ],
                  ),
                ),
                const Spacer(),
                Padding(
                  padding: EdgeInsets.only(right: 20.si, bottom: 10.si),
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: state is LoadingPhoneAuthState
                        ? SizedBox.square(
                            dimension: 35.si,
                            child: const CircularProgressIndicator(),
                          )
                        : state is InputCodeState
                            ? FloatingActionButton(
                                child: const Icon(Icons.navigate_next_rounded),
                                onPressed: () {
                                  if (validOtp(otpCodeController.text)) {
                                    final bloc = context.read<PhoneAuthBloc>();

                                    bloc.add(
                                      PhoneAuthEvent.verifySentOtp(
                                        otpCode: otpCodeController.text,
                                        verificationId:
                                            (bloc.state as InputCodeState)
                                                .verificationId,
                                      ),
                                    );
                                  } else {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                        content: Text('Invalid otp !'),
                                      ),
                                    );
                                  }
                                },
                              )
                            : const SizedBox.shrink(),
                  ),
                ),
                SizedBox(
                  height: 10.si,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
