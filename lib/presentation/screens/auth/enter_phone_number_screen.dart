import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yum/domain/auth/blocks/phone_auth_bloc.dart';
import 'package:yum/presentation/app/screen_size_helper.dart';
import 'package:yum/presentation/routes/main_router.dart';
import 'package:yum/presentation/screens/map/custom_back_button.dart';

class EnterPhoneNumberScreen extends StatefulWidget {
  const EnterPhoneNumberScreen({super.key});

  @override
  State<EnterPhoneNumberScreen> createState() => _EnterPhoneNumberScreenState();
}

class _EnterPhoneNumberScreenState extends State<EnterPhoneNumberScreen> {
  final TextEditingController phoneNumberController = TextEditingController();

  bool validPhone(String phoneNumber) {
    return phoneNumber.isNotEmpty;
  }

  @override
  void dispose() {
    phoneNumberController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<PhoneAuthBloc, PhoneAuthState>(
      listener: (context, state) {
        if (state is InputCodeState) {
          context.router.push(
            EnterOtpCodeRoute(
              phoneNumber: phoneNumberController.text,
            ),
          );
        }
      },
      builder: (context, state) {
        return Scaffold(
          body: SafeArea(
            child: Center(
              child: Column(
                children: [
                  const Align(
                    alignment: Alignment.topLeft,
                    child: CustomBackButton(
                      icon: Icons.close_rounded,
                    ),
                  ),
                  const Spacer(),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20.si),
                    child: Column(
                      children: [
                        Text(
                          'Your phone number',
                          style: Theme.of(context).textTheme.headline1,
                        ),
                        SizedBox(
                          height: 10.si,
                        ),
                        Text(
                          'Please confirm your country code'
                          '\nand enter your phone number',
                          style: Theme.of(context).textTheme.bodyText1,
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          height: 30.si,
                        ),
                        TextField(
                          keyboardType: TextInputType.phone,
                          autofocus: true,
                          decoration: InputDecoration(
                            labelStyle: TextStyle(
                              color: Theme.of(context).primaryColor,
                            ),
                            floatingLabelBehavior: FloatingLabelBehavior.always,
                            labelText: 'Phone number',
                            hintText: 'Enter phone number',
                          ),
                          controller: phoneNumberController,
                        )
                      ],
                    ),
                  ),
                  const Spacer(),
                  Padding(
                    padding: EdgeInsets.only(right: 20.si, bottom: 10.si),
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: state is LoadingPhoneAuthState
                          ? SizedBox.square(
                              dimension: 35.si,
                              child: const CircularProgressIndicator(),
                            )
                          : FloatingActionButton(
                              child: const Icon(Icons.navigate_next_rounded),
                              onPressed: () {
                                if (validPhone(phoneNumberController.text)) {
                                  context.read<PhoneAuthBloc>().add(
                                        PhoneAuthEvent.sendOtpToPhone(
                                          phoneNumber:
                                              phoneNumberController.text,
                                        ),
                                      );
                                } else {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                      content: Text('Invalid phone number !'),
                                    ),
                                  );
                                }
                              },
                            ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
