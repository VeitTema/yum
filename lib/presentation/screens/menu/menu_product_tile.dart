import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yum/domain/cart/blocs/cart_cubit.dart';
import 'package:yum/domain/menu/models/product.dart';
import 'package:yum/presentation/app/extensions/text_style_extensions.dart';
import 'package:yum/presentation/app/screen_size_helper.dart';
import 'package:yum/presentation/routes/main_router.dart';
import 'package:yum/presentation/widgets/button.dart';

class MenuProductTile extends StatelessWidget {
  MenuProductTile({
    super.key,
    required this.product,
    required this.onTap,
  });

  final Product product;
  final GlobalKey imageGlobalKey = GlobalKey();
  final void Function(GlobalKey) onTap;

  static const buttonCaptionTextStyle = TextStyle(
    height: 1.27,
    fontSize: 14,
    letterSpacing: 0.06,
    fontWeight: FontWeight.w400,
  );

  static const descriptionTextStyle = TextStyle(
    height: 1.2,
    fontSize: 12,
    letterSpacing: 0.09,
    fontWeight: FontWeight.w400,
  );

  @override
  Widget build(BuildContext context) {
    return InkWell(
      overlayColor: MaterialStateProperty.all(
        Theme.of(context).primaryColor.withOpacity(0.15),
      ),
      onTap: () {
        context.router.push(ProductRoute(product: product));
      },
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 20.si,
          vertical: 10.si,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox.square(
              dimension: 125.si,
              child: DecoratedBox(
                decoration: BoxDecoration(
                  border: Border.all(color: Theme.of(context).primaryColor),
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(25),
                ),
                child: Container(
                  key: imageGlobalKey,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: CachedNetworkImage(
                      imageUrl: product.imageUrl,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 125.si,
              width: 210.si,
              child: Column(
                children: [
                  Text(
                    product.title,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 16.si,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Expanded(
                    child: Text(
                      // product.description,
                      'Пастрами из индейки, острые колбаски чоризо, пикантная пепперони, бекон, моцарелла, томатный соус',
                      style: descriptionTextStyle.fo
                          .copyWith(color: Colors.black87),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      context.read<CartCubit>().addToCart(product);
                      onTap(imageGlobalKey);
                    },
                    borderRadius: BorderRadius.circular(20),
                    overlayColor: MaterialStateProperty.all(
                      Theme.of(context).primaryColor.withOpacity(0.15),
                    ),
                    child: Container(
                      height: 35.si,
                      width: 100.si,
                      padding: EdgeInsets.symmetric(
                          horizontal: 10.si, vertical: 8.si),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Theme.of(context).primaryColor.withOpacity(0.15),
                      ),
                      child: Text(
                        '${product.cost}p',
                        style: buttonCaptionTextStyle.fo.copyWith(
                          color: Theme.of(context).primaryColor,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
