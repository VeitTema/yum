import 'package:flutter/material.dart';
import 'package:yum/presentation/app/extensions/text_style_extensions.dart';
import 'package:yum/presentation/app/screen_size_helper.dart';

class MenuNavBar extends StatelessWidget {
  const MenuNavBar({
    super.key,
    required this.currentIndex,
    required this.onTap,
    required this.items,
    required this.keys,
    this.selectedColor,
    this.unselectedColor,
  });

  final int currentIndex;
  final void Function(GlobalKey key, int index) onTap;
  final List<MenuNavBarItem> items;
  final Map<String, GlobalKey> keys;

  final Color? selectedColor;
  final Color? unselectedColor;

  static const textStyle = TextStyle(
    height: 1.27,
    fontSize: 14,
    letterSpacing: 0.06,
    fontWeight: FontWeight.w400,
  );

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      elevation: 1,
      toolbarHeight: 50.si,
      backgroundColor: Theme.of(context).backgroundColor,
      title: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: List<Widget>.generate(items.length, (index) {
            final color = index == currentIndex
                ? selectedColor ??
                    Theme.of(context).primaryColor.withOpacity(0.15)
                : unselectedColor ??
                    Theme.of(context).disabledColor.withOpacity(0.15);
            return GestureDetector(
              onTap: () {
                onTap(keys[items[index].label]!, index);
              },
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 5.si),
                padding:
                    EdgeInsets.symmetric(horizontal: 10.si, vertical: 8.si),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: color,
                ),
                child: Text(
                  items[index].label,
                  style: textStyle.fo.copyWith(
                    color: index == currentIndex
                        ? Theme.of(context).primaryColor
                        : Theme.of(context).disabledColor,
                  ),
                ),
              ),
            );
          }),
        ),
      ),
      pinned: true,
    );
  }
}

class MenuNavBarItem {
  const MenuNavBarItem({
    required this.label,
  });
  final String label;
}
