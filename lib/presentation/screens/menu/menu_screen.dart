import 'package:add_to_cart_animation/add_to_cart_animation.dart';
import 'package:add_to_cart_animation/add_to_cart_icon.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yum/domain/map/blocs/map_cubit.dart';
import 'package:yum/domain/menu/blocs/menu_cubit.dart';
import 'package:yum/domain/menu/blocs/news_cubit.dart';
import 'package:yum/main.dart';
import 'package:yum/presentation/app/screen_size_helper.dart';
import 'package:yum/presentation/routes/main_router.dart';
import 'package:yum/presentation/screens/home/cart_key_inherited.dart';
import 'package:yum/presentation/screens/menu/menu_nav_bar.dart';
import 'package:yum/presentation/screens/menu/menu_product_list.dart';
import 'package:yum/presentation/screens/news/news_list.dart';

class MenuScreen extends StatefulWidget {
  const MenuScreen({super.key});

  @override
  State<MenuScreen> createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {
  final scrollController = ScrollController();
  final ValueNotifier<int> currentMenuIndex = ValueNotifier(0);

  final Map<String, GlobalKey> keys = {};
  bool listen = true;

  void setActive(int index) {
    if (currentMenuIndex.value != index) {
      currentMenuIndex.value = index;
    }
  }

  Future<void> scrollToItem(GlobalKey key, int index) async {
    listen = false;
    setActive(index);
    await Scrollable.ensureVisible(
      key.currentContext!,
      duration: const Duration(milliseconds: 300),
      curve: Curves.easeInOut,
    );
    listen = true;
  }

  void checkMenuNavBarIndex() {
    if (listen) {
      final categoryLengthList = <double>[];
      for (var index = 0;
          index < context.read<MenuCubit>().state.payload.categories.length;
          index++) {
        var categorySize = 125.si;
        for (var i = 0; i < index + 1; i++) {
          categorySize += 145.si *
              context
                  .read<MenuCubit>()
                  .state
                  .payload
                  .categories[i]
                  .products
                  .length;
        }
        categoryLengthList.add(categorySize);
      }
      for (var index = 0; index < categoryLengthList.length; index++) {
        if (scrollController.offset < categoryLengthList[index]) {
          setActive(index);
          break;
        }
      }
    }
  }

  late Future<void> Function(GlobalKey) runAddToCardAnimation;

  Future<void> addToCartAnimation(GlobalKey gkImageContainer) async {
    await runAddToCardAnimation(gkImageContainer);
  }

  @override
  void initState() {
    super.initState();
    scrollController.addListener(checkMenuNavBarIndex);
  }

  @override
  void dispose() {
    super.dispose();
    scrollController.dispose();
    currentMenuIndex.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<MapCubit, MapState>(
      listener: (context, state) {
        if ((state.isSuccess || state.isFailure) &&
            state.payload.savedPoint == null) {
          context.router.push(const MapRoute());
        }
      },
      builder: (context, state) {
        return AddToCartAnimation(
          gkCart: CartKeyInherited.of(context).cartKey,
          rotation: true,
          initiaJump: false,
          receiveCreateAddToCardAnimationMethod: (addToCardAnimationMethod) {
            runAddToCardAnimation = addToCardAnimationMethod;
          },
          child: Scaffold(
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(35.si),
              child: AppBar(
                centerTitle: false,
                title: state.isSuccess && state.payload.savedPoint != null
                    ? GestureDetector(
                        onTap: () {
                          context.router.push(const MapRoute());
                        },
                        child: Padding(
                          padding: EdgeInsets.only(left: 16.si),
                          child: Row(
                            children: [
                              ConstrainedBox(
                                constraints: BoxConstraints(maxWidth: 270.si),
                                child: Text(
                                  state.payload.savedPoint?.address ?? '',
                                  style: const TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                  ),
                                  overflow: TextOverflow.fade,
                                ),
                              ),
                              const Icon(Icons.arrow_drop_down_rounded),
                            ],
                          ),
                        ),
                      )
                    : null,
                actions: [
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.search,
                    ),
                  )
                ],
              ),
            ),
            body: BlocBuilder<MenuCubit, MenuState>(
              builder: (context, menuState) {
                if (menuState.isInitial || menuState.isLoading) {
                  return const Center(child: CircularProgressIndicator());
                }
                if (menuState.isFailure) {
                  return const Center(
                    child: Text('Error'),
                  );
                }
                if (menuState.isSuccess) {
                  for (var i = 0;
                      i < menuState.payload.categories.length;
                      i++) {
                    keys[menuState.payload.categories[i].categoryName] =
                        GlobalKey();
                  }
                  return SafeArea(
                    child: CustomScrollView(
                      physics: const BouncingScrollPhysics(),
                      controller: scrollController,
                      slivers: [
                        BlocBuilder<NewsCubit, NewsState>(
                          builder: (context, newsState) {
                            if (newsState.isSuccess) {
                              return NewsList(
                                newsList: newsState.payload,
                              );
                            }
                            return const SliverToBoxAdapter();
                          },
                        ),
                        ValueListenableBuilder(
                          valueListenable: currentMenuIndex,
                          builder: (context, val, child) {
                            return MenuNavBar(
                              currentIndex: currentMenuIndex.value,
                              onTap: scrollToItem,
                              keys: keys,
                              items: List.generate(
                                menuState.payload.categories.length,
                                (index) => MenuNavBarItem(
                                  label: menuState
                                      .payload.categories[index].categoryName,
                                ),
                              ),
                            );
                          },
                        ),
                        MenuProductList(
                          onTap: addToCartAnimation,
                          productMenu: menuState.payload,
                          keys: keys,
                        ),
                      ],
                    ),
                  );
                }
                return const SizedBox.shrink();
              },
            ),
          ),
        );
      },
    );
  }
}
