import 'package:flutter/material.dart';
import 'package:yum/domain/menu/models/product_menu.dart';
import 'package:yum/presentation/screens/menu/menu_product_tile.dart';

class MenuProductList extends StatelessWidget {
  const MenuProductList({
    super.key,
    required this.productMenu,
    required this.keys,
    required this.onTap,
  });

  final ProductMenu productMenu;
  final void Function(GlobalKey) onTap;

  final Map<String, GlobalKey> keys;

  List<Widget> buildProductList() {
    final productList = <Widget>[];

    for (final category in productMenu.categories) {
      for (var index = 0; index < category.products.length; index++) {
        productList.add(
          MenuProductTile(
            onTap: onTap,
            key: index == 0 ? keys[category.categoryName] : null,
            product: category.products[index],
          ),
        );
      }
    }
    return productList;
  }

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Column(
        children: buildProductList(),
      ),
    );
  }
}
