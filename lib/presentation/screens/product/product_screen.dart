import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yum/domain/cart/blocs/cart_cubit.dart';
import 'package:yum/domain/menu/models/product.dart';
import 'package:yum/presentation/app/screen_size_helper.dart';
import 'package:yum/presentation/screens/map/custom_back_button.dart';
import 'package:yum/presentation/widgets/button.dart';

class ProductScreen extends StatelessWidget {
  const ProductScreen({super.key, required this.product});

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: [
            Stack(
              children: [
                SizedBox(
                  height: 300.si,
                  child: CachedNetworkImage(
                    imageUrl: product.imageUrl,
                    fit: BoxFit.cover,
                  ),
                ),
                const SafeArea(
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: CustomBackButton(
                      backgroundColor: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
            SafeArea(
              top: false,
              child: Padding(
                padding: EdgeInsets.all(20.si),
                child: Column(
                  children: [
                    Text(
                      product.title,
                      style: Theme.of(context).textTheme.headline1,
                    ),
                    SizedBox(
                      height: 10.si,
                    ),
                    Text(
                      product.description,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      bottomSheet: Container(
        width: double.infinity,
        height: 80.si,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.si),
            topRight: Radius.circular(20.si),
          ),
          boxShadow: [
            BoxShadow(
              color: Theme.of(context).primaryColor,
              blurRadius: 0.5,
              offset: const Offset(0, -2),
            )
          ],
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 5.si,
          vertical: 5.si,
        ),
        child: Button(
          onPressed: () {
            context.read<CartCubit>().addToCart(product);
          },
          child: Text(
            'To Cart',
            style: Theme.of(context).textTheme.button,
          ),
        ),
      ),
    );
  }
}
