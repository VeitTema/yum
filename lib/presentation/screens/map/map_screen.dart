import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yum/domain/map/blocs/map_cubit.dart';
import 'package:yum/presentation/app/screen_size_helper.dart';
import 'package:yum/presentation/screens/map/custom_back_button.dart';
import 'package:yum/presentation/screens/map/custom_map.dart';
import 'package:yum/presentation/widgets/button.dart';

class MapScreen extends StatefulWidget {
  const MapScreen({super.key});

  @override
  State<MapScreen> createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  @override
  void initState() {
    if (context.read<MapCubit>().state.payload.pointList.isEmpty) {
      context.read<MapCubit>().fetchPoints();
    }
    super.initState();
  }

  Future<void> tryingToSavePoint() async {
    final mapCubit = context.read<MapCubit>();

    if (mapCubit.state.payload.selectedPoint != null) {
      await mapCubit
          .saveSelectedPoint(
        mapCubit.state.payload.selectedPoint!,
      )
          .then((_) async {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            duration: Duration(seconds: 1, milliseconds: 500),
            behavior: SnackBarBehavior.floating,
            dismissDirection: DismissDirection.horizontal,
            content: Text('Point saved!'),
          ),
        );
        await Future.delayed(
          const Duration(
            seconds: 2,
          ),
        ).then((_) {
          context.router.pop();
        });
      });
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Select point!'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          BlocBuilder<MapCubit, MapState>(builder: (context, state) {
            if (state.isSuccess) {
              return const CustomMap();
            }
            return const SizedBox.shrink();
          }),
          SafeArea(
            child: Column(
              children: [
                BlocBuilder<MapCubit, MapState>(builder: (context, state) {
                  if (state.payload.savedPoint != null) {
                    return const Align(
                      alignment: Alignment.topLeft,
                      child: CustomBackButton(
                        backgroundColor: Colors.white,
                      ),
                    );
                  }
                  return const SizedBox.shrink();
                }),
                const Spacer(),
                Container(
                  width: double.infinity,
                  height: 300.si,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.si),
                      topRight: Radius.circular(20.si),
                    ),
                    boxShadow: const [
                      BoxShadow(
                        color: Colors.grey,
                        blurRadius: 0.5,
                        offset: Offset(0, -2),
                      )
                    ],
                  ),
                  padding: EdgeInsets.only(
                    right: 20.si,
                    left: 20.si,
                    top: 10.si,
                    bottom: 30.si,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.only(bottom: 5.si),
                        child: const Text(
                          'Self-delivery',
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                      BlocBuilder<MapCubit, MapState>(
                        builder: (context, state) {
                          if (state.isLoading || state.isInitial) {
                            return const Expanded(
                              child: Center(
                                child: CircularProgressIndicator(),
                              ),
                            );
                          }

                          if (state.isFailure) {
                            return const Expanded(
                              child: Center(
                                child: Text('Error'),
                              ),
                            );
                          }

                          if (state.isSuccess) {
                            return Expanded(
                              child: ListView.builder(
                                shrinkWrap: true,
                                physics: const BouncingScrollPhysics(),
                                itemCount: state.payload.pointList.length,
                                itemBuilder: (context, index) {
                                  final point = state.payload.pointList[index];
                                  return SizedBox(
                                    width: double.infinity,
                                    height: 40.si,
                                    child: Row(
                                      children: [
                                        Radio(
                                          value: point.id,
                                          groupValue:
                                              state.payload.selectedPoint?.id,
                                          onChanged: (_) {
                                            context
                                                .read<MapCubit>()
                                                .selectPoint(point);
                                          },
                                        ),
                                        Expanded(
                                          child: Text(
                                            point.address,
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                },
                              ),
                            );
                          }

                          return const SizedBox.shrink();
                        },
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10.si),
                        child: Button(
                          onPressed: tryingToSavePoint,
                          padding: EdgeInsets.zero,
                          child: const Text('Select'),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
