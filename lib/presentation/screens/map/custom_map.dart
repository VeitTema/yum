import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:yum/domain/map/blocs/map_cubit.dart';
import 'package:yum/domain/map/models/point.dart';
import 'package:yum/presentation/screens/map/custom_marker_drawer.dart';

class CustomMap extends StatefulWidget {
  const CustomMap({super.key});

  @override
  CustomMapState createState() => CustomMapState();
}

class CustomMapState extends State<CustomMap> {
  GoogleMapController? mapController;
  static const LatLng centerPoint =
      LatLng(53.87135358988266, 27.563234896535565);
  final Set<Marker> markers = {};
  final CustomMarkerDrawer markerDrawer = CustomMarkerDrawer();
  double scale = 1;

  void selectPlace(Point place) {
    context.read<MapCubit>().selectPoint(place);
    mapPlacesToMarkers();
  }

  Future<void> upsertMarker(Point place) async {
    final selectedPlace = context.read<MapCubit>().state.payload.selectedPoint;
    final isSelected = place.id == selectedPlace?.id;

    final icon = await markerDrawer.createCustomMarkerBitmap(
      isSelected: isSelected,
      scale: scale,
    );

    markers.add(
      Marker(
        markerId: MarkerId(place.id),
        position: place.location,
        onTap: () => selectPlace(place),
        icon: icon.marker,
        anchor: Offset(
          icon.anchorDx,
          icon.anchorDy,
        ),
      ),
    );
  }

  Future<void> mapPlacesToMarkers() async {
    final points = context.read<MapCubit>().state.payload.pointList;
    for (final point in points) {
      await upsertMarker(point);
    }

    setState(() {});
  }

  void onMapCreated(GoogleMapController controller) {
    setState(() {
      mapController = controller;
    });

    rootBundle.loadString('assets/map_style.json').then((mapStyle) {
      mapController?.setMapStyle(mapStyle);
    });
  }

  @override
  void initState() {
    super.initState();

    Future.delayed(
      Duration.zero,
      () {
        scale = MediaQuery.of(context).devicePixelRatio;

        mapPlacesToMarkers();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<MapCubit, MapState>(
      listener: (context, state) {
        mapPlacesToMarkers();
      },
      child: GoogleMap(
        onMapCreated: onMapCreated,
        initialCameraPosition: const CameraPosition(
          target: centerPoint,
          zoom: 11,
        ),
        markers: markers,
      ),
    );
  }
}
