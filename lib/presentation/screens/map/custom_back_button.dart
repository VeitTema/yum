import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:yum/presentation/app/screen_size_helper.dart';

class CustomBackButton extends StatelessWidget {
  const CustomBackButton({
    super.key,
    this.backgroundColor,
    this.icon,
  });

  @protected
  final IconData? icon;

  @protected
  final Color? backgroundColor;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        context.router.pop();
      },
      child: Container(
        margin: EdgeInsets.all(10.si),
        padding: EdgeInsets.only(
          right: 7.si,
          left: 5.si,
          bottom: 7.si,
          top: 7.si,
        ),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: backgroundColor,
        ),
        child: Icon(
          icon ?? Icons.arrow_back_ios_new_rounded,
          size: 25.si,
        ),
      ),
    );
  }
}
