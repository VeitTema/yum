import 'dart:async';
import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/material.dart' hide Image;
import 'package:google_maps_flutter/google_maps_flutter.dart';

class CustomMarker {
  CustomMarker({
    required this.marker,
    required this.anchorDy,
    required this.anchorDx,
  });
  final BitmapDescriptor marker;
  final double anchorDy;
  final double anchorDx;
}

class CustomMarkerDrawer {
  static const double canvasWidth = 98;
  static const double canvasHeight = 64;

  static const double defaultIconWidth = 60;
  static const double defaultIconHeight = 40;

  static const String defaultPath = 'assets/icons/map_place.png';
  static const String defaultSelectedPath =
      'assets/icons/selected_map_place.png';

  Future<CustomMarker> createCustomMarkerBitmap({
    required bool isSelected,
    required double scale,
  }) async {
    final scaledCanvasWidth = canvasWidth * scale;
    final scaledCanvasHeight = canvasHeight * scale;

    final recorder = PictureRecorder();
    final canvas = Canvas(
      recorder,
      Rect.fromLTWH(
        0,
        0,
        scaledCanvasWidth,
        scaledCanvasHeight,
      ),
    );

    final imageHeight = await paintMarkerImage(
      scaledCanvasWidth: scaledCanvasWidth,
      canvas: canvas,
      scale: scale,
      isSelected: isSelected,
    );

    final picture = recorder.endRecording();
    final pngBytes = await (await picture.toImage(
      scaledCanvasWidth.toInt(),
      scaledCanvasHeight.toInt(),
    ))
        .toByteData(format: ImageByteFormat.png);

    final data = Uint8List.view(pngBytes!.buffer);

    final marker = BitmapDescriptor.fromBytes(data);
    const anchorDx = .5;
    final anchorDy = imageHeight / scaledCanvasHeight;

    return CustomMarker(marker: marker, anchorDx: anchorDx, anchorDy: anchorDy);
  }

  /// Return drawed image height
  Future<double> paintMarkerImage({
    required Canvas canvas,
    required bool isSelected,
    required double scale,
    required double scaledCanvasWidth,
  }) async {
    final myImage = await getImage(isSelected: isSelected);

    final double imageWidth;
    final double imageHeight;

    if (isSelected) {
      imageWidth = defaultIconWidth * scale;
      imageHeight = defaultIconHeight * scale;
    } else {
      imageWidth = defaultIconWidth * scale;
      imageHeight = defaultIconHeight * scale;
    }

    final imageDx = (scaledCanvasWidth - imageWidth) / 2;
    const imageDy = 0.0;

    paintImage(
      canvas: canvas,
      image: myImage,
      rect: Rect.fromLTWH(
        imageDx,
        imageDy,
        imageWidth,
        imageHeight,
      ),
    );

    return imageHeight;
  }

  Future<Image> getImage({
    required bool isSelected,
  }) async {
    final completer = Completer<ImageInfo>();

    final String path;

    if (isSelected) {
      path = defaultSelectedPath;
    } else {
      path = defaultPath;
    }

    final img = AssetImage(path);

    img.resolve(const ImageConfiguration()).addListener(
      ImageStreamListener((info, _) {
        completer.complete(info);
      }),
    );

    final imageInfo = await completer.future;

    return imageInfo.image;
  }
}
