import 'package:add_to_cart_animation/add_to_cart_icon.dart';
import 'package:flutter/material.dart';

class CartKeyInherited extends InheritedWidget {
  const CartKeyInherited({
    super.key,
    required this.cartKey,
    required super.child,
  });

  final GlobalKey<CartIconKey> cartKey;

  static CartKeyInherited of(BuildContext context) {
    final result =
        context.dependOnInheritedWidgetOfExactType<CartKeyInherited>();
    assert(result != null, 'No CartKeyInherited found in context');
    return result!;
  }

  @override
  bool updateShouldNotify(CartKeyInherited oldWidget) =>
      cartKey != oldWidget.cartKey;
}
