import 'package:add_to_cart_animation/add_to_cart_icon.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:yum/main.dart';
import 'package:yum/presentation/routes/main_router.dart';
import 'package:yum/presentation/screens/home/cart_key_inherited.dart';
import 'package:yum/presentation/widgets/bottom_nav_bar.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final cartKey = GlobalKey<CartIconKey>();
    return AutoTabsRouter(
      routes: const [
        MenuRoute(),
        ProfileRoute(),
        CartRoute(),
      ],
      builder: (context, child, animation) {
        final tabsRouter = AutoTabsRouter.of(context);

        return CartKeyInherited(
          cartKey: cartKey,
          child: Scaffold(
            body: FadeTransition(
              opacity: animation,
              child: child,
            ),
            bottomNavigationBar: BottomNavBar(
              currentIndex: tabsRouter.activeIndex,
              onTap: tabsRouter.setActiveIndex,
              items: [
                const BottomNavBarItem(
                  iconData: Icons.menu_book,
                  label: 'Menu',
                ),
                const BottomNavBarItem(
                  iconData: Icons.person,
                  label: 'Profile',
                ),
                BottomNavBarItem(
                  key: cartKey,
                  iconData: Icons.shopping_cart_outlined,
                  label: 'Cart',
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
