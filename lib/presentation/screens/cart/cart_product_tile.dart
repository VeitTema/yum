import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yum/domain/cart/blocs/cart_cubit.dart';
import 'package:yum/domain/cart/models/cart_product.dart';
import 'package:yum/presentation/app/screen_size_helper.dart';
import 'package:yum/presentation/screens/cart/plus_minus_button_widget.dart';

class CartProductTile extends StatelessWidget {
  const CartProductTile({
    super.key,
    required this.product,
  });

  final CartProduct product;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: 20.si,
        vertical: 20.si,
      ),
      child: Column(
        children: [
          Row(
            children: [
              SizedBox.square(
                dimension: 90.si,
                child: DecoratedBox(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Theme.of(context).primaryColor,
                    ),
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(25),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: CachedNetworkImage(
                      imageUrl: product.imageUrl,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 10.si,
                  ),
                  height: 100.si,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        product.title,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16.si,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Text(
                        product.description,
                        style: TextStyle(
                          color: Colors.black87,
                          fontSize: 14.si,
                          fontWeight: FontWeight.w400,
                        ),
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10.si,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              PlusMinusButton(
                addQuantity: () {
                  context.read<CartCubit>().addToCart(product);
                },
                deleteQuantity: () {
                  context.read<CartCubit>().deleteFromCart(product);
                },
                text: '${product.count}',
              ),
              Text(
                '${product.cost * product.count}p',
                style: const TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
