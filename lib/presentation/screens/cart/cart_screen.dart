import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yum/domain/cart/blocs/cart_cubit.dart';
import 'package:yum/domain/cart/models/cart_product.dart';
import 'package:yum/presentation/app/screen_size_helper.dart';
import 'package:yum/presentation/screens/cart/cart_bottom_sheet.dart';
import 'package:yum/presentation/screens/cart/cart_product_tile.dart';
import 'package:yum/presentation/widgets/button.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({super.key});

  @override
  State<CartScreen> createState() => _CartScreenState();
}

int getProductCount(List<CartProduct> productList) {
  var count = 0;
  for (final product in productList) {
    count += product.count;
  }
  return count;
}

class _CartScreenState extends State<CartScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CartCubit, CartState>(
      builder: (context, state) {
        final cartProducts = state.payload;
        if (state.isInitial || state.isLoading) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        if (state.isFailure) {
          return const Center(
            child: Text('Failure'),
          );
        }
        if (state.isSuccess) {
          return Scaffold(
            appBar: AppBar(
              centerTitle: false,
              title: Padding(
                padding: EdgeInsets.only(left: 16.si, top: 14.si),
                child: Text(
                  cartProducts.isEmpty
                      ? 'Cart'
                      : 'Cart (${getProductCount(cartProducts)})',
                ),
              ),
            ),
            body: SafeArea(
              child: Center(
                child: cartProducts.isEmpty
                    ? Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          const Text(
                            'Empty cart, make an order ',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Button(
                            stretch: false,
                            child: const Text('Go to menu'),
                            onPressed: () {
                              AutoTabsRouter.of(context).setActiveIndex(0);
                            },
                          ),
                        ],
                      )
                    : Column(
                        children: [
                          Expanded(
                            child: ListView.separated(
                              shrinkWrap: true,
                              physics: const BouncingScrollPhysics(),
                              itemCount: cartProducts.length,
                              itemBuilder: (context, index) {
                                final product = cartProducts[index];
                                return CartProductTile(product: product);
                              },
                              separatorBuilder:
                                  (BuildContext context, int index) {
                                return const Divider(
                                  thickness: 2,
                                );
                              },
                            ),
                          ),
                          CartBottomSheet(
                            productList: cartProducts,
                          ),
                        ],
                      ),
              ),
            ),
          );
        }
        return const SizedBox.shrink();
      },
    );
  }
}
