import 'package:auto_route/auto_route.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yum/domain/cart/models/cart_product.dart';
import 'package:yum/domain/map/blocs/map_cubit.dart';
import 'package:yum/presentation/app/screen_size_helper.dart';
import 'package:yum/presentation/routes/main_router.dart';
import 'package:yum/presentation/widgets/button.dart';

// Animated BottomSheet When tap button animate to up.

class CartBottomSheet extends StatefulWidget {
  const CartBottomSheet({
    super.key,
    required this.productList,
  });

  final List<CartProduct> productList;

  @override
  State<CartBottomSheet> createState() => _CartBottomSheetState();
}

class _CartBottomSheetState extends State<CartBottomSheet> {
  int getTotalPrice(List<CartProduct> productList) {
    var totalPrice = 0;
    for (final product in productList) {
      totalPrice += product.count * product.cost;
    }
    return totalPrice;
  }

  int? orderingOption;
  int? paymentMethod;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 80.si,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20.si),
          topRight: Radius.circular(20.si),
        ),
        boxShadow: [
          BoxShadow(
            color: Theme.of(context).primaryColor,
            blurRadius: 0.5,
            offset: const Offset(0, -2),
          )
        ],
      ),
      padding: EdgeInsets.symmetric(
        horizontal: 5.si,
        vertical: 5.si,
      ),
      child: Button(
        child: Text(
          'Place an order by ${getTotalPrice(widget.productList)}p',
          style: Theme.of(context).textTheme.button,
        ),
        onPressed: () {
          if (FirebaseAuth.instance.currentUser != null) {
            showModalBottomSheet<void>(
              backgroundColor: Colors.transparent,
              context: context,
              builder: (BuildContext context) {
                return StatefulBuilder(
                  builder: (BuildContext context, StateSetter setState) {
                    return Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: 20.si,
                        vertical: 20.si,
                      ),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20),
                        ),
                        color: Colors.white,
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'At the restaurant',
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                          SizedBox(
                            height: 5.si,
                          ),
                          Text(context
                              .read<MapCubit>()
                              .state
                              .payload
                              .savedPoint!
                              .address),
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 10.si),
                            child: const Divider(
                              thickness: 2,
                            ),
                          ),
                          Text(
                            'Ordering option',
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                          SizedBox(
                            height: 5.si,
                          ),
                          SizedBox(
                            width: double.infinity,
                            height: 30.si,
                            child: Row(
                              children: [
                                Radio(
                                  value: 0,
                                  groupValue: orderingOption,
                                  onChanged: (value) {
                                    setState(() {
                                      orderingOption = value;
                                    });
                                  },
                                ),
                                const Expanded(
                                  child: Text('Bring to the table'),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: double.infinity,
                            height: 30.si,
                            child: Row(
                              children: [
                                Radio(
                                  value: 1,
                                  groupValue: orderingOption,
                                  onChanged: (value) {
                                    setState(() {
                                      orderingOption = value;
                                    });
                                  },
                                ),
                                const Expanded(
                                  child: Text('Pick up restaurant cash desk'),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 10.si),
                            child: const Divider(
                              thickness: 2,
                            ),
                          ),
                          Text(
                            'Payment method ',
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                          SizedBox(
                            height: 5.si,
                          ),
                          SizedBox(
                            width: double.infinity,
                            height: 30.si,
                            child: Row(
                              children: [
                                Radio(
                                  value: 0,
                                  groupValue: paymentMethod,
                                  onChanged: (value) {
                                    setState(() {
                                      paymentMethod = value;
                                    });
                                  },
                                ),
                                const Expanded(
                                  child: Text('Card in the app'),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: double.infinity,
                            height: 30.si,
                            child: Row(
                              children: [
                                Radio(
                                  value: 1,
                                  groupValue: paymentMethod,
                                  onChanged: (value) {
                                    setState(() {
                                      paymentMethod = value;
                                    });
                                  },
                                ),
                                const Expanded(
                                  child: Text('Card in a restaurant'),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: double.infinity,
                            height: 30.si,
                            child: Row(
                              children: [
                                Radio(
                                  value: 2,
                                  groupValue: paymentMethod,
                                  onChanged: (value) {
                                    setState(() {
                                      paymentMethod = value;
                                    });
                                  },
                                ),
                                const Expanded(
                                  child: Text('Cash in a restaurant'),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 10.si),
                            child: const Divider(
                              thickness: 2,
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Order price',
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                              Text(
                                '${getTotalPrice(widget.productList)}p',
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 15.si,
                          ),
                          Button(
                            padding: EdgeInsets.zero,
                            child: const Text('Make an order'),
                            onPressed: () =>
                                // AutoTabsRouter.of(context).setActiveIndex(1)
                                Navigator.pop(context),
                          ),
                        ],
                      ),
                    );
                  },
                );
              },
            );
          } else {
            context.router.push(const AuthenticationFlow());
          }
        },
      ),
    );

    // return Container(
    //   width: double.infinity,
    //   height: 90.si,
    //   decoration: BoxDecoration(
    //     color: Colors.white,
    //     borderRadius: BorderRadius.only(
    //       topLeft: Radius.circular(20.si),
    //       topRight: Radius.circular(20.si),
    //     ),
    //     border: Border.all(),
    //     boxShadow: [
    //       BoxShadow(
    //         color: Theme.of(context).primaryColor,
    //         blurRadius: 0.5,
    //         offset: const Offset(0, -2),
    //       )
    //     ],
    //   ),
    //   padding: EdgeInsets.symmetric(
    //     horizontal: 20.si,
    //     vertical: 10.si,
    //   ),
    //   child: Button(
    //     onPressed: () {
    //       if (FirebaseAuth.instance.currentUser != null) {
    //         onPressed;
    //       } else {
    //         context.router.push(const AuthenticationFlow());
    //       }
    //     },
    //     child: Text(
    //       'Place an order by ${getTotalPrice(productList)}p',
    //       style: Theme.of(context).textTheme.button,
    //     ),
    //   ),
    // );
  }
}
