import 'package:flutter/material.dart';
import 'package:yum/presentation/app/screen_size_helper.dart';

class PlusMinusButton extends StatelessWidget {
  const PlusMinusButton({
    super.key,
    required this.addQuantity,
    required this.deleteQuantity,
    required this.text,
  });
  final VoidCallback deleteQuantity;
  final VoidCallback addQuantity;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 90.si,
      height: 40.si,
      padding: EdgeInsets.symmetric(horizontal: 8.si, vertical: 4.si),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        border: Border.all(color: Theme.of(context).primaryColor),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            onTap: deleteQuantity,
            borderRadius: BorderRadius.circular(20),
            child: Icon(
              Icons.remove,
              size: 26.si,
              color: Theme.of(context).primaryColor,
            ),
          ),
          Text(
            text,
            style: const TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w500,
            ),
          ),
          InkWell(
            onTap: addQuantity,
            borderRadius: BorderRadius.circular(20),
            child: Icon(
              Icons.add,
              size: 26.si,
              color: Theme.of(context).primaryColor,
            ),
          ),
        ],
      ),
    );
  }
}
