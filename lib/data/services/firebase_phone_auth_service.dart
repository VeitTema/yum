// ignore_for_file: inference_failure_on_function_return_type
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:injectable/injectable.dart';
import 'package:yum/domain/auth/model/user.dart';
import 'package:yum/domain/auth/services/phone_auth_service.dart';

@Injectable(as: PhoneAuthService)
class FirebasePhoneAuthService implements PhoneAuthService {
  FirebaseAuth auth = FirebaseAuth.instance;
  @override
  bool get isUserAuth => auth.currentUser == null;

  @override
  Future<void> verifyPhone({
    required String phoneNumber,
    required Function(PhoneAuthCredential) verificationCompleted,
    required Function(FirebaseAuthException) verificationFailed,
    required Function(String, int?) codeSent,
    required Function(String) codeAutoRetrievalTimeout,
  }) async {
    await auth.verifyPhoneNumber(
      phoneNumber: phoneNumber,
      verificationCompleted: verificationCompleted,
      verificationFailed: verificationFailed,
      codeSent: codeSent,
      codeAutoRetrievalTimeout: codeAutoRetrievalTimeout,
    );
  }

  @override
  PhoneAuthCredential getCredential({
    required String verificationId,
    required String smsCode,
  }) {
    final credential = PhoneAuthProvider.credential(
      verificationId: verificationId,
      smsCode: smsCode,
    );
    return credential;
  }

  @override
  Future<void> signInWithCredential({
    required AuthCredential credential,
  }) async {
    await auth.signInWithCredential(
      credential,
    );
    await addUserToDataBase();
  }

  Future<void> addUserToDataBase() async {
    final user = await FirebaseFirestore.instance
        .collection('users')
        .doc(auth.currentUser!.uid)
        .get();

    if (!user.exists) {
      await FirebaseFirestore.instance
          .collection('users')
          .doc(auth.currentUser!.uid)
          .set(
            UserModel(
              phone: auth.currentUser!.phoneNumber!,
              id: auth.currentUser!.uid,
            ).toJson(),
          );
    }
  }
}
