import 'package:yum/app/helpers/result.dart';

import 'package:yum/domain/menu/models/product_menu.dart';
import 'package:yum/domain/menu/services/menu_service.dart';

class FakeMenuService implements MenuService {
  const FakeMenuService();

  @override
  Future<Result<ErrorLoadingMenu, ProductMenu>> fetchProductMenu() async {
    try {
      await Future.delayed(const Duration(seconds: 1));
      final productMenu = ProductMenu.fromMap(ProductMenu.menu);

      return Result.value(productMenu);
    } catch (_) {
      return const Result.error(ErrorLoadingMenu.unknown);
    }
  }
}
