import 'package:yum/app/helpers/result.dart';
import 'package:yum/domain/menu/models/news.dart';
import 'package:yum/domain/menu/services/news_service.dart';

class FakeNewsService implements NewsService {
  const FakeNewsService();

  @override
  Future<Result<ErrorLoadingNews, List<News>>> fetchNews() async {
    try {
      await Future.delayed(const Duration(seconds: 1));
      final newsList = News.newsList.map(News.fromMap).toList();
      return Result.value(newsList);
    } catch (_) {
      return const Result.error(ErrorLoadingNews.unknown);
    }
  }
}
