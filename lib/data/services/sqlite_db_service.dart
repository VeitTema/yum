import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:yum/domain/cart/models/cart_product.dart';
import 'package:yum/domain/cart/services/cart_service.dart';
import 'package:yum/domain/menu/models/product.dart';

class SQLiteDbService implements CartService {
  SQLiteDbService._();
  static final SQLiteDbService db = SQLiteDbService._();
  static Database? _database;

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await initDB();
    return _database!;
  }

  Future<Database> initDB() async {
    final documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(
      documentsDirectory.path,
      'Cart.db',
    );
    return openDatabase(
      path,
      version: 1,
      onOpen: (db) {},
      onCreate: (Database db, int version) async {
        await db.execute('CREATE TABLE Cart ('
            'id INTEGER PRIMARY KEY,'
            'count INTEGER,'
            'title TEXT,'
            'description TEXT,'
            'cost INTEGER,'
            'imageUrl TEXT'
            ')');
      },
    );
  }

  @override
  Future<List<CartProduct>> getAllProducts() async {
    final db = await database;
    final List<Map<String, dynamic>> results = await db.query('Cart');
    final cartProducts = <CartProduct>[];
    for (final result in results) {
      final cartProduct = CartProduct.fromMap(result);
      cartProducts.add(cartProduct);
    }
    return cartProducts;
  }

  @override
  Future<CartProduct?> findProductByTitle(String title) async {
    final db = await database;
    final List<Map<String, dynamic>> results =
        await db.query('Cart', where: 'title = ?', whereArgs: [title]);
    return results.isNotEmpty ? CartProduct.fromMap(results.first) : null;
  }

  @override
  Future<int> insert(Product product) async {
    final db = await database;
    final table = await db.rawQuery('SELECT MAX(id)+1 as id FROM Cart');
    final id = table.first['id'];

    final result = await db.rawInsert(
      'INSERT Into Cart (id,count,title,description,cost,imageUrl)'
      ' VALUES (?,?,?,?,?,?)',
      [
        id,
        1,
        product.title,
        product.description,
        product.cost,
        product.imageUrl,
      ],
    );

    return result;
  }

  @override
  Future<int> update(CartProduct cartProduct) async {
    final db = await database;
    final result = await db.update(
      'Cart',
      cartProduct.toMap(),
      where: 'id = ?',
      whereArgs: [cartProduct.id],
    );
    return result;
  }

  @override
  Future<void> delete(int id) async {
    final db = await database;
    await db.delete(
      'Cart',
      where: 'id = ?',
      whereArgs: [id],
    );
  }
}
