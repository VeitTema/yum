import 'package:yum/app/helpers/result.dart';
import 'package:yum/domain/map/services/map_service.dart';
import 'package:yum/domain/map/models/point.dart';

class FakeMapService extends MapService {
  const FakeMapService();

  @override
  Future<Result<ErrorLoadingPoints, List<Point>>> fetchPoints() async {
    await Future.delayed(const Duration(seconds: 1));

    try {
      final pointList = Point.pointList.map(Point.fromMap).toList();
      return Result.value(pointList);
    } catch (_) {
      return const Result.error(ErrorLoadingPoints.unknown);
    }
  }
}
